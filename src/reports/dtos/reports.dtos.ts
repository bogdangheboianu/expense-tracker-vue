export interface TotalSpentQueryParamsDto {
    fromDate?: Date;
    toDate?: Date;
}

export interface TotalEarnedQueryParamsDto {
    fromDate?: Date;
    toDate?: Date;
}

export interface TotalSpentDto {
    sum: number;
}

export interface TotalEarnedDto {
    sum: number;
}

export interface SpentOnCategoriesQueryParamsDto {
    fromDate?: string;
    toDate?: string;
}

export interface SpentOnSellersQueryParamsDto {
    fromDate?: string;
    toDate?: string;
}

export interface SpentOnCategoryDto {
    categoryName: string;
    categorySlug: string;
    sum: number;
}

export interface SpentOnSellerDto {
    sellerName: string;
    sellerSlug: string;
    sum: number;
}

export interface MonthlyReportDto {
    month: Date;
    totalSpent?: number;
    totalEarned?: number;
}
