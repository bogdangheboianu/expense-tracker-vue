import { apiEndpoints } from '@/core/http/constants/api-endpoints.constants';
import { get } from '@/core/http/functions/http.functions';
import { IApiResponse } from '@/core/http/interfaces/api-response.interface';
import { MonthlyReportDto, SpentOnCategoriesQueryParamsDto, SpentOnCategoryDto, SpentOnSellerDto, SpentOnSellersQueryParamsDto, TotalEarnedDto, TotalEarnedQueryParamsDto, TotalSpentDto, TotalSpentQueryParamsDto } from '@/reports/dtos/reports.dtos';

export class ReportsApiService {

    static getTotalSpent(params: TotalSpentQueryParamsDto): Promise<IApiResponse<TotalSpentDto>> {
        return get( apiEndpoints.reports.totalSpent, params );
    }

    static getTotalEarned(params: TotalEarnedQueryParamsDto): Promise<IApiResponse<TotalEarnedDto>> {
        return get( apiEndpoints.reports.totalEarned, params );
    }

    static getSpentOnCategories(params: SpentOnCategoriesQueryParamsDto): Promise<IApiResponse<SpentOnCategoryDto[]>> {
        return get( apiEndpoints.reports.spentOnCategories, params );
    }

    static getSpentOnSellers(params: SpentOnSellersQueryParamsDto): Promise<IApiResponse<SpentOnSellerDto[]>> {
        return get( apiEndpoints.reports.spentOnSellers, params );
    }

    static getAllMonthlyReports(): Promise<IApiResponse<MonthlyReportDto[]>> {
        return get( apiEndpoints.reports.monthlyReports );
    }
}
