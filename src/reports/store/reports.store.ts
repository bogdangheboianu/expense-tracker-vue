import { StateErrors, StoreStatusEnum } from '@/config/constants/store.constants';
import { MonthlyReportDto, SpentOnCategoriesQueryParamsDto, SpentOnCategoryDto, SpentOnSellerDto, SpentOnSellersQueryParamsDto, TotalEarnedQueryParamsDto, TotalSpentQueryParamsDto } from '@/reports/dtos/reports.dtos';
import { ReportsApiService } from '@/reports/services/api/reports-api.service';
import { ErrorDto } from '@/shared/dtos/error.dtos';
import { isDefined } from '@/shared/functions/validation.functions';
import { defineStore } from 'pinia';

export interface ReportsState {
    totalSpent: {
        sum: number | null;
        status: StoreStatusEnum
    };
    totalEarned: {
        sum: number | null;
        status: StoreStatusEnum
    };
    spentOnCategories: {
        data: SpentOnCategoryDto[];
        status: StoreStatusEnum;
    };
    spentOnSellers: {
        data: SpentOnSellerDto[];
        status: StoreStatusEnum;
    };
    monthlyReports: {
        data: MonthlyReportDto[];
        status: StoreStatusEnum;
    };
    errors: StateErrors<ReportsState>;
}

export const useReportsStore = defineStore( 'reports', {
    state  : (): ReportsState => (
        {
            totalSpent       : {
                sum   : null,
                status: StoreStatusEnum.Initial
            },
            totalEarned      : {
                sum   : null,
                status: StoreStatusEnum.Initial
            },
            spentOnCategories: {
                data  : [],
                status: StoreStatusEnum.Initial
            },
            spentOnSellers   : {
                data  : [],
                status: StoreStatusEnum.Initial
            },
            monthlyReports   : {
                data  : [],
                status: StoreStatusEnum.Initial
            },
            errors           : {
                totalSpent       : null,
                totalEarned      : null,
                spentOnCategories: null,
                spentOnSellers   : null,
                monthlyReports   : null
            }
        }
    ),
    getters: {
        totalSumSpent                  : (state: ReportsState): number | null => state.totalSpent.sum,
        totalSumSpentIsLoading         : (state: ReportsState): boolean => state.totalSpent.status === StoreStatusEnum.Loading,
        totalSumEarned                 : (state: ReportsState): number | null => state.totalEarned.sum,
        totalSumEarnedIsLoading        : (state: ReportsState): boolean => state.totalEarned.status === StoreStatusEnum.Loading,
        totalSpentOnCategories         : (state: ReportsState): SpentOnCategoryDto[] => state.spentOnCategories.data,
        totalSpentOnCategoriesIsLoading: (state: ReportsState): boolean => state.spentOnCategories.status === StoreStatusEnum.Loading,
        totalSpentOnSellers            : (state: ReportsState): SpentOnSellerDto[] => state.spentOnSellers.data,
        totalSpentOnSellersIsLoading   : (state: ReportsState): boolean => state.spentOnSellers.status === StoreStatusEnum.Loading,
        monthlyReportsList             : (state: ReportsState): MonthlyReportDto[] => state.monthlyReports.data,
        monthlyReportsAreLoading       : (state: ReportsState): boolean => state.monthlyReports.status === StoreStatusEnum.Loading
    },
    actions: {
        async loadTotalSpent(params: TotalSpentQueryParamsDto): Promise<void> {
            this.onLoading( 'totalSpent' );
            const response = await ReportsApiService.getTotalSpent( params );
            this.$patch( { totalSpent: { sum: response.data?.sum } } );
            this.onLoadingFinished( 'totalSpent', response.error );
        },
        async loadTotalEarned(params: TotalEarnedQueryParamsDto): Promise<void> {
            this.onLoading( 'totalEarned' );
            const response = await ReportsApiService.getTotalEarned( params );
            this.$patch( { totalEarned: { sum: response.data?.sum } } );
            this.onLoadingFinished( 'totalEarned', response.error );
        },
        async loadSpentOnCategories(params: SpentOnCategoriesQueryParamsDto): Promise<void> {
            this.onLoading( 'spentOnCategories' );
            const response = await ReportsApiService.getSpentOnCategories( params );
            this.$patch( { spentOnCategories: { data: response.data ?? [] } } );
            this.onLoadingFinished( 'spentOnCategories', response.error );
        },
        async loadSpentOnSellers(params: SpentOnSellersQueryParamsDto): Promise<void> {
            this.onLoading( 'spentOnSellers' );
            const response = await ReportsApiService.getSpentOnSellers( params );
            this.$patch( { spentOnSellers: { data: response.data ?? [] } } );
            this.onLoadingFinished( 'spentOnSellers', response.error );
        },
        async loadAllMonthlyReports(): Promise<void> {
            this.onLoading( 'monthlyReports' );
            const response = await ReportsApiService.getAllMonthlyReports();
            this.$patch( { monthlyReports: { data: response.data ?? [] } } );
            this.onLoadingFinished( 'monthlyReports', response.error );
        },
        onLoading(storeKey: keyof ReportsState) {
            this.$patch(
                {
                    [storeKey]: {
                        status: StoreStatusEnum.Loading
                    },
                    errors    : {
                        [storeKey]: null
                    }
                }
            );
        },
        onLoadingFinished(storeKey: keyof ReportsState, error: ErrorDto | null) {
            this.$patch(
                {
                    [storeKey]: {
                        status: isDefined( error )
                                ? StoreStatusEnum.Failed
                                : StoreStatusEnum.Success
                    },
                    errors    : {
                        [storeKey]: error
                    }
                }
            );
        }
    }
} );
