import { RouteRecordRaw } from 'vue-router';

export const publicRoutes: RouteRecordRaw[] = [
    {
        path     : '/',
        name     : 'Home',
        component: () => import('@/public/pages/HomePage.vue')
    }
];

export const notFoundRoute: RouteRecordRaw = {
    path     : '/:pathMatch(.*)*',
    name     : 'NotFound',
    component: () => import('@/public/pages/NotFoundPage.vue')
};
