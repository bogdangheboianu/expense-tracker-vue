export enum RecurrenceStatusEnum {
    Active   = 'active',
    Inactive = 'inactive'
}
