import { InputProps } from '@/shared/interfaces/input-props/input-props.interface';
import { FormRules } from 'element-plus';

export type FormFields<T> = Record<keyof T, InputProps<any, T>>

export type ValidationRules = FormRules;

export interface ValidationRulesConfig<F> {
    required?: boolean;
    equalsToField?: keyof F;
}

export type FormFieldsValidationRulesConfig<T> = Record<keyof T, ValidationRulesConfig<T> | null>
