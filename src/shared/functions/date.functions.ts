import { ExpenseRecurrenceDto } from '@/expense/dtos/expense-recurrence.dtos';
import { IncomeRecurrenceDto } from '@/income/dtos/income-recurrence.dtos';
import { DateFormat } from '@/shared/enums/date-format.enum';
import { WeekDayEnum } from '@/shared/enums/week-day.enum';
import { isDefined } from '@/shared/functions/validation.functions';
import { DateObj } from '@/shared/interfaces/date/date-obj.interface';
import moment, { CalendarSpec, Moment } from 'moment';

const humanizedDateCalendarFormats: CalendarSpec = {
    sameDay : '[Today]',
    nextDay : '[Tomorrow]',
    nextWeek: 'dddd',
    lastDay : '[Yesterday]',
    lastWeek: '[Last] dddd',
    sameElse: DateFormat.ForDisplay
};

export const formatDate = (date: Date | string | null | undefined, format: DateFormat = DateFormat.ForServer): string | undefined => {
    return isDefined( date )
           ? moment( date )
               .format( format )
               .toString()
           : undefined;
};

export const humanizeDate = (date: Date | Moment): string => {
    return date instanceof Date
           ? moment( date )
               .calendar( null, humanizedDateCalendarFormats )
           : date.calendar( null, humanizedDateCalendarFormats );
};

export const humanizeUpcomingDate = (recurrence: ExpenseRecurrenceDto | IncomeRecurrenceDto): DateObj | null => {
    const daily: boolean | null = recurrence.daily;
    const weeklyOn: WeekDayEnum | null = recurrence.weeklyOn;
    const monthlyOn: number | null = recurrence.monthlyOn;
    const today = moment();
    const tomorrow = today.add( 1, 'days' );
    const todayHour = today.toDate()
                           .getHours();
    const todayWeekDay = today.toDate()
                              .getDay();
    const todayMonthDay = today.toDate()
                               .getDate();

    if( daily || (
        weeklyOn && weeklyOn === todayWeekDay
    ) || (
        monthlyOn && monthlyOn === todayMonthDay
    ) ) {
        if( todayHour < 12 ) {
            return { date: today.toDate(), humanizedDate: humanizeDate( today ) };
        } else {
            return { date: tomorrow.toDate(), humanizedDate: humanizeDate( tomorrow ) };
        }
    }

    if( weeklyOn ) {
        if( weeklyOn > todayWeekDay ) {
            const day = today.add( weeklyOn - todayWeekDay, 'days' );
            return { date: day.toDate(), humanizedDate: humanizeDate( day ) };
        } else {
            const day = today.add( 7 + (
                weeklyOn - todayWeekDay
            ), 'days' );
            return { date: day.toDate(), humanizedDate: humanizeDate( day ) };
        }
    }

    if( monthlyOn && monthlyOn > todayMonthDay && monthlyOn - todayMonthDay <= 14 ) {
        const day = today.add( monthlyOn - todayMonthDay, 'days' );
        return { date: day.toDate(), humanizedDate: humanizeDate( day ) };
    }

    return null;
};

export const disabledFutureDates = (date: Date): boolean => date.getTime() > Date.now();
