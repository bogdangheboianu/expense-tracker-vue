import { isEmpty, isNil } from 'lodash-es';

export function isDefined<T>(input: null | undefined | T): input is T {
    return input !== null && input !== undefined;
}

export function valueIsEmpty(value: Object | string | number | null | undefined): value is null | undefined {

    if( typeof value === 'number' ) {
        return isNil( value );
    }

    if( value instanceof Date ) {
        return false;
    }

    return isNil( value ) || isEmpty( value );
}
