import { isDefined } from '@/shared/functions/validation.functions';

export const parseIntegerMoneyValueToDecimalString = (moneyValue?: number | null): string | null => {
    return isDefined( moneyValue )
           ? (
               moneyValue / 100
           ).toFixed( 2 )
           : null;
};

export const parseIntegerMoneyValueToDecimal = (moneyValue?: number | null): number | null => {
    return isDefined( moneyValue )
           ? Number( (
                         moneyValue / 100
                     ).toFixed( 2 ) )
           : null;
};

export const parseDecimalMoneyValueToInteger = (moneyValue?: number | null): number | null => {
    return isDefined( moneyValue )
           ? Math.round( moneyValue * 100 )
           : null;
};
