export const calculateCurrentPage = (limit: number, offset: number): number => {
    return (
        offset + limit
    ) / limit;
};

export const calculateOffset = (currentPage: number, limit: number): number => {
    return currentPage * limit - limit;
};
