import { FormFields, FormFieldsValidationRulesConfig, ValidationRulesConfig } from '@/shared/types/form.types';
import { FormRules } from 'element-plus/es';

export const buildValidationRules = <T>(formFields: FormFields<T>): FormRules => {
    const rules: any = {};
    const config = validationRulesConfig( formFields );

    Object.keys( config )
          .forEach( key => {
              const formField = key as keyof FormFieldsValidationRulesConfig<T>;
              const fieldConfig: ValidationRulesConfig<T> | null = config[formField];
              if( !fieldConfig ) {
                  return;
              }
              if( fieldConfig.required ) {
                  rules[formField] = [ { required: true, message: 'Required field', trigger: [ 'blur', 'change', 'input' ] } ];
              }

              if( fieldConfig.equalsToField ) {
                  rules[formField] = [
                      ...rules[formField],
                      {
                          validator: (rule: any, value: any, callback: any) => {
                              if( value !== formFields[fieldConfig.equalsToField!].value ) {
                                  callback( new Error( 'Fields don\'t match' ) );
                              }
                              callback();
                          },
                          trigger  : [ 'blur', 'change', 'input' ]
                      }
                  ];
              }
          } );

    return rules as FormRules;
};

const validationRulesConfig = <F>(formFields: FormFields<F>): FormFieldsValidationRulesConfig<F> => {
    const rules: any = {};

    Object.keys( formFields )
          .forEach( (key: string) => {
              const formFieldKey = key as keyof FormFields<F>;
              rules[formFieldKey] = formFields[formFieldKey].rulesConfig ?? {};
          } );

    return rules as FormFieldsValidationRulesConfig<F>;
};
