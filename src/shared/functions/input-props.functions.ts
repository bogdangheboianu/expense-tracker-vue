import { CurrencyEnum } from '@/shared/enums/currency.enum';
import { FormFieldTypeEnum } from '@/shared/enums/form-field-type.enum';
import { CheckboxInputProps } from '@/shared/interfaces/input-props/checkbox-input-props.interface';
import { DateInputProps } from '@/shared/interfaces/input-props/date-input-props.interface';
import { DateRangeInputProps } from '@/shared/interfaces/input-props/date-range-props.interface';
import { InputProps } from '@/shared/interfaces/input-props/input-props.interface';
import { MoneyInputProps } from '@/shared/interfaces/input-props/money-input-props.interface';
import { NumberInputProps } from '@/shared/interfaces/input-props/number-input-props.interface';
import { PasswordInputProps } from '@/shared/interfaces/input-props/password-input-props.interface';
import { TextInputProps } from '@/shared/interfaces/input-props/text-input-props.interface';
import { ref } from 'vue';

export const buildInputProps = <T, F>(type: FormFieldTypeEnum, label: string, prop: keyof F, value: T | null = null, required: boolean = true, equalsToProp?: keyof F): InputProps<T, F> => (
    { type, label, prop, value: ref(value), rulesConfig: { required, equalsToField: equalsToProp } }
);

export const textInputProps = <F>(label: string, prop: keyof F, value: string | null = null, required: boolean = true): TextInputProps<F> => (
    {
        ...buildInputProps( FormFieldTypeEnum.Text, label, prop, value, required )
    }
);

export const numberInputProps = <F>(label: string, prop: keyof F, value: number | null = null, min?: number, max?: number, step?: number, required: boolean = true): NumberInputProps<F> => (
    {
        ...buildInputProps( FormFieldTypeEnum.Number, label, prop, value, required ),
        min, max, step
    }
);

export const moneyInputProps = <F>(label: string, prop: keyof F, value: number | null = null, currency: CurrencyEnum = CurrencyEnum.RON, required: boolean = true): MoneyInputProps<F> => (
    {
        ...buildInputProps( FormFieldTypeEnum.Money, label, prop, value, required ),
        currency
    }
);

export const dateInputProps = <F>(label: string, prop: keyof F, value: Date | null = null, disabledDates?: (date: Date) => boolean, required: boolean = true): DateInputProps<F> => (
    {
        ...buildInputProps( FormFieldTypeEnum.Date, label, prop, value, required ),
        disabledDates
    }
);

export const dateRangeInputProps = <F>(label: string, prop: keyof F, startDate: Date | null = null, endDate: Date | null = null, required: boolean = true): DateRangeInputProps<F> => (
    {
        ...buildInputProps( FormFieldTypeEnum.DateRange, label, prop, { startDate, endDate }, required )
    }
);

export const passwordInputProps = <F>(label: string, prop: keyof F, equalsToProp?: keyof F): PasswordInputProps<F> => (
    {
        ...buildInputProps( FormFieldTypeEnum.Password, label, prop, null, true, equalsToProp )
    }
);

export const checkboxInputProps = <F>(label: string, prop: keyof F, value: boolean = false): CheckboxInputProps<F> => (
    {
        ...buildInputProps( FormFieldTypeEnum.Checkbox, label, prop, value, false )
    }
);
