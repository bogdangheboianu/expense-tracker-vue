export enum FormFieldTypeEnum {
    Text      = 'text',
    Number    = 'number',
    Date      = 'date',
    Money     = 'money',
    DateRange = 'dateRange',
    Seller    = 'seller',
    Category  = 'category',
    Password  = 'password',
    Checkbox  = 'checkbox'
}
