export enum DateFormat {
    ForServer  = 'YYYY-MM-DD',
    ForDisplay = 'DD/MM/YYYY'
}
