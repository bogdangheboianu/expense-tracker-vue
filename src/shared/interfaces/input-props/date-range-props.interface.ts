import { DateRange } from '@/shared/interfaces/date/date-range.interface';
import { InputProps } from '@/shared/interfaces/input-props/input-props.interface';

export interface DateRangeInputProps<F> extends InputProps<DateRange, F> {
}
