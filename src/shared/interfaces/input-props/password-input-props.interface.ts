import { InputProps } from '@/shared/interfaces/input-props/input-props.interface';

export interface PasswordInputProps<F> extends InputProps<string, F> {

}
