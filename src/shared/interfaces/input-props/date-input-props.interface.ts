import { InputProps } from '@/shared/interfaces/input-props/input-props.interface';

export interface DateInputProps<F> extends InputProps<Date, F> {
    disabledDates?: (date: Date) => boolean;
}
