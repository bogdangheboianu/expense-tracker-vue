import { InputProps } from '@/shared/interfaces/input-props/input-props.interface';

export interface TextInputProps<F> extends InputProps<string, F> {
}
