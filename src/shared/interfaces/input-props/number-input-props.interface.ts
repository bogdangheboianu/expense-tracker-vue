import { InputProps } from '@/shared/interfaces/input-props/input-props.interface';

export interface NumberInputProps<F> extends InputProps<number, F> {
    min?: number;
    max?: number;
    step?: number;
}
