import { CurrencyEnum } from '@/shared/enums/currency.enum';
import { InputProps } from '@/shared/interfaces/input-props/input-props.interface';

export interface MoneyInputProps<F> extends InputProps<number, F> {
    currency?: CurrencyEnum;
}
