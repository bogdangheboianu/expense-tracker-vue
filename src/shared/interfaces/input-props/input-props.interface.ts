import { FormFieldTypeEnum } from '@/shared/enums/form-field-type.enum';
import { ValidationRulesConfig } from '@/shared/types/form.types';
import { Ref, UnwrapRef } from 'vue';

export interface InputProps<T, F> {
    type: FormFieldTypeEnum;
    label: string;
    prop: keyof F;
    value: Ref<UnwrapRef<T> | null>;
    placeholder?: string;
    rulesConfig?: ValidationRulesConfig<F>;
}
