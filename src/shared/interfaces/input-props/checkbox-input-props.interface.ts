import { InputProps } from '@/shared/interfaces/input-props/input-props.interface';

export interface CheckboxInputProps<F> extends InputProps<boolean, F> {
}
