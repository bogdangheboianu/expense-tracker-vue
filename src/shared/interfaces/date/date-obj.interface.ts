export interface DateObj {
    date: Date;
    humanizedDate: string;
}
