import { ErrorDto } from '@/shared/dtos/error.dtos';

export interface IApiResponse<T> {
    data: T | null;
    error: ErrorDto | null;
}
