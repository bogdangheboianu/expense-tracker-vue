import { useAuthStore } from '@/auth/store/auth.store';
import { apiBaseURL } from '@/core/http/constants/api-endpoints.constants';
import { IApiResponse } from '@/core/http/interfaces/api-response.interface';
import { ErrorDto } from '@/shared/dtos/error.dtos';
import { isDefined } from '@/shared/functions/validation.functions';
import axios, { AxiosError, AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios';
import applyCaseMiddleware from 'axios-case-converter';

const http = (requiresAuth: boolean): AxiosInstance => {
    const axiosRequestConfig: AxiosRequestConfig = {
        baseURL: apiBaseURL,
        timeout: 6000
    };
    const authStore = useAuthStore();

    if( requiresAuth && authStore.isAuthenticated ) {
        axiosRequestConfig.headers = { 'Authorization': `Bearer ${ authStore.accessToken! }` };
    }

    const axiosInstance = axios.create( axiosRequestConfig );

    return applyCaseMiddleware( axiosInstance );
};

const handleError = (error: AxiosError<ErrorDto>): IApiResponse<any> => {
    const isServerError = isDefined( error.response );
    const noResponseError = isDefined( error.request );
    const detailedError = error.toJSON();
    const response: IApiResponse<any> = { data: null, error: null };

    if( isServerError ) {
        console.log( `Server error (${ error.response!.status }): ${ error.response!.data }\nDetailed error: ${ detailedError }` );
        return { ...response, error: error.response!.data };
    }

    if( noResponseError ) {
        console.log( `Server unavailable. Request: ${ error.request }\nDetailed error: ${ detailedError }` );
        return { ...response, error: { detail: 'Server unavailable' } };
    }

    console.log( `Error: ${ error.message }\nDetailed error: ${ detailedError }` );
    return { ...response, error: { detail: error.message } };
};

export const get = async <T>(uri: string, params: any = {}, requiresAuth: boolean = true): Promise<IApiResponse<T>> => {
    return http( requiresAuth )
        .get<T>( uri, { params } )
        .then( (response: AxiosResponse<T>) => (
            { data: response.data, error: null }
        ) )
        .catch( handleError );
};

export const post = async <T, R>(uri: string, data: T, requiresAuth: boolean = true): Promise<IApiResponse<R>> => {
    return http( requiresAuth )
        .post<R, AxiosResponse<R>, T>( uri, data )
        .then( (response: AxiosResponse<R>) => (
            { data: response.data, error: null }
        ) )
        .catch( handleError );
};

export const put = async <T, R>(uri: string, data: T, requiresAuth: boolean = true): Promise<IApiResponse<R>> => {
    return http( requiresAuth )
        .put<R, AxiosResponse<R>, T>( uri, data )
        .then( (response: AxiosResponse<R>) => (
            { data: response.data, error: null }
        ) )
        .catch( handleError );
};

export const del = async <R>(uri: string, requiresAuth: boolean = true): Promise<IApiResponse<R>> => {
    return http( requiresAuth )
        .delete<R, AxiosResponse<R>>( uri )
        .then( (response: AxiosResponse<R>) => (
            { data: response.data, error: null }
        ) )
        .catch( handleError );
};

export const patch = async <T, R>(uri: string, data: T | null = null, requiresAuth: boolean = true): Promise<IApiResponse<R>> => {
    return http( requiresAuth )
        .patch<R, AxiosResponse<R>, T | null>( uri, data )
        .then( (response: AxiosResponse<R>) => (
            { data: response.data, error: null }
        ) )
        .catch( handleError );
};
