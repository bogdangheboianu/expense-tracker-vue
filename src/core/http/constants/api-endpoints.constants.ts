export const apiBaseURL = process.env.NODE_ENV === 'development'
                          ? 'http://localhost:8000/api'
                          : 'https://xpnstrkr.herokuapp.com/api';

export const apiEndpoints = {
    auth      : {
        login  : '/auth/jwt/create/',
        refresh: '/auth/jwt/refresh/'
    },
    users     : {
        create: '/users/',
        me    : '/users/me/'
    },
    setup     : {
        steps: '/setup/steps/'
    },
    expenses  : {
        list                  : '/expenses/',
        create                : '/expenses/create/',
        update                : '/expenses/update/',
        delete                : '/expenses/delete/',
        bulkDelete            : '/expenses/delete/bulk/',
        exclude               : '/expenses/exclude/',
        bulkExclude           : '/expenses/exclude/bulk/',
        values                : '/expenses/values/',
        createValue           : '/expenses/values/create/',
        updateValue           : '/expenses/values/update/',
        deleteValue           : '/expenses/values/delete/',
        recurrences           : '/expenses/recurrences/',
        createRecurrence      : '/expenses/recurrences/create/',
        updateRecurrence      : '/expenses/recurrences/update/',
        updateRecurrenceStatus: '/expenses/recurrences/update/status/',
        deleteRecurrence      : '/expenses/recurrences/delete/'
    },
    incomes   : {
        list                  : '/incomes/',
        create                : '/incomes/create/',
        update                : '/incomes/update/',
        delete                : '/incomes/delete/',
        recurrences           : '/incomes/recurrences/',
        createRecurrence      : '/incomes/recurrences/create/',
        updateRecurrence      : '/incomes/recurrences/update/',
        updateRecurrenceStatus: '/incomes/recurrences/update/status/',
        deleteRecurrence      : '/incomes/recurrences/delete/'
    },
    budgets   : {
        create: '/budgets/create/'
    },
    sellers   : {
        list: '/sellers/'
    },
    categories: {
        list: '/categories/'
    },
    reports   : {
        monthlyReports   : '/reports/monthly-reports/',
        totalSpent       : '/reports/spent/total/',
        spentOnCategories: '/reports/spent/categories/',
        spentOnSellers   : '/reports/spent/sellers/',
        totalEarned      : '/reports/earned/total/'
    }
};
