import { authRoutes } from '@/auth/routes/auth.routes';
import { useAuthStore } from '@/auth/store/auth.store';
import { dashboardRoute } from '@/dashboard/routes';
import { expenseRoutes } from '@/expense/routes/expense.routes';
import { incomeRoutes } from '@/income/routes/income.routes';
import { notFoundRoute, publicRoutes } from '@/public/routes/public.routes';
import { setupRoutes } from '@/user/routes/setup.routes';
import { useSetupStore } from '@/user/store/setup.store';
import { createRouter, createWebHistory, NavigationGuardNext, RouteLocationNormalized } from 'vue-router';

export const router = createRouter(
    {
        history: createWebHistory( import.meta.env.BASE_URL ),
        routes : [
            ...publicRoutes,
            ...authRoutes,
            ...setupRoutes,
            dashboardRoute,
            ...expenseRoutes,
            ...incomeRoutes,
            notFoundRoute
        ]
    }
);

router.beforeEach( (to: RouteLocationNormalized, from: RouteLocationNormalized, next: NavigationGuardNext) => {
    const authStore = useAuthStore();
    const setupStore = useSetupStore();

    const isAuthenticated = authStore.isAuthenticated;
    const requiresAuthAndIsNotAuthenticated = to.meta.requiresAuth && !isAuthenticated;
    const isAuthRouteAndIsAuthenticated = to.name === 'Auth' && isAuthenticated;
    const requiresSetupCompleteAndIsNot = to.meta.requiresSetupComplete && !setupStore.setupIsComplete;

    if( requiresAuthAndIsNotAuthenticated ) {
        next( { name: 'Auth' } );
    } else if( isAuthRouteAndIsAuthenticated ) {
        next( { name: 'Dashboard' } );
    } else if( requiresSetupCompleteAndIsNot ) {
        next( { name: 'Setup' } );
    } else if( to.name === 'Setup' && setupStore.setupIsComplete ) {
        next( { name: 'Dashboard' } );
    } else {
        next();
    }
} );
