import { SellerDto } from '@/seller/dtos/seller.dtos';
import { SellerInputProps } from '@/seller/interfaces/seller-input-props.interface';
import { FormFieldTypeEnum } from '@/shared/enums/form-field-type.enum';
import { buildInputProps } from '@/shared/functions/input-props.functions';

export const sellerInputProps = <F>(label: string, prop: keyof F, value: SellerDto | null = null, required: boolean = true): SellerInputProps<F> => (
    {
        ...buildInputProps( FormFieldTypeEnum.Seller, label, prop, value?.id, required ),
        seller: value ?? undefined
    }
);
