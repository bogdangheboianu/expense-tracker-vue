import { SellerDto } from '@/seller/dtos/seller.dtos';
import { InputProps } from '@/shared/interfaces/input-props/input-props.interface';

export interface SellerInputProps<F> extends InputProps<string, F> {
    seller?: SellerDto;
}
