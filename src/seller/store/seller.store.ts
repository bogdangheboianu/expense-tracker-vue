import { SellerApiService } from '@/seller/services/api/seller-api.service';
import { SellerDto } from '@/seller/dtos/seller.dtos';
import { ErrorDto } from '@/shared/dtos/error.dtos';
import { isDefined } from '@/shared/functions/validation.functions';
import { defineStore } from 'pinia';

export interface SellerState {
    list: SellerDto[];
    loading: boolean;
    success: boolean;
    error: ErrorDto | null;
}

export const useSellerStore = defineStore( 'seller', {
    state  : (): SellerState => (
        {
            list   : [],
            loading: false,
            success: false,
            error  : null
        }
    ),
    actions: {
        async searchSellers(searchQuery: string): Promise<SellerDto[]> {
            this.onLoading();
            const response = await SellerApiService.getSellers( searchQuery );
            const sellers = response.data ?? [];
            this.$patch( { list: sellers } );
            this.onLoadingFinished( response.error );
            return sellers;
        },
        onLoading(): void {
            this.$patch( { loading: true, success: false, error: null } );
        },
        onLoadingFinished(error: ErrorDto | null): void {
            this.$patch( { loading: false, success: !isDefined( error ), error } );
        }
    }
} );
