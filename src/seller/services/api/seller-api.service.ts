import { apiEndpoints } from '@/core/http/constants/api-endpoints.constants';
import { get } from '@/core/http/functions/http.functions';
import { IApiResponse } from '@/core/http/interfaces/api-response.interface';
import { SellerDto } from '@/seller/dtos/seller.dtos';
import { isDefined } from '@/shared/functions/validation.functions';

export class SellerApiService {

    static async getSellers(searchQuery?: string): Promise<IApiResponse<SellerDto[]>> {
        const uri = `${ apiEndpoints.sellers.list }${ isDefined( searchQuery )
                                                      ? `?search_query=${ searchQuery }`
                                                      : '' }`;
        return get( uri );
    }
}
