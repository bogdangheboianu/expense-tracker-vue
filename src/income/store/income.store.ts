import { StateErrors, StoreStatusEnum } from '@/config/constants/store.constants';
import { CreateIncomeDto, DeleteIncomeDto, IncomeDto, IncomeListQueryParamsDto, UpdateIncomeDto } from '@/income/dtos/income.dtos';
import { IncomeApiService } from '@/income/services/api/income-api.service';
import { IncomeListQueryParamsCacheService } from '@/income/services/cache/income-list-query-params-cache.service';
import { ErrorDto } from '@/shared/dtos/error.dtos';
import { formatDate } from '@/shared/functions/date.functions';
import { isDefined } from '@/shared/functions/validation.functions';
import moment from 'moment';
import { defineStore } from 'pinia';

export interface IncomeState {
    list: {
        data: IncomeDto[];
        total: number;
        status: StoreStatusEnum;
    };
    recentList: {
        data: IncomeDto[];
        status: StoreStatusEnum;
    };
    create: {
        status: StoreStatusEnum;
    };
    update: {
        status: StoreStatusEnum;
    };
    delete: {
        status: StoreStatusEnum;
    };
    errors: StateErrors<IncomeState>;
}

export const useIncomeStore = defineStore( 'income', {
    state  : (): IncomeState => (
        {
            list      : {
                data  : [],
                total : 0,
                status: StoreStatusEnum.Initial
            },
            recentList: {
                data  : [],
                status: StoreStatusEnum.Initial
            },
            create    : {
                status: StoreStatusEnum.Initial
            },
            update    : {
                status: StoreStatusEnum.Initial
            },
            delete    : {
                status: StoreStatusEnum.Initial
            },
            errors    : {
                list      : null,
                create    : null,
                recentList: null,
                delete    : null,
                update    : null
            }
        }
    ),
    getters: {
        incomes                : (state: IncomeState): IncomeDto[] => state.list.data,
        totalIncomes           : (state: IncomeState): number => state.list.total,
        incomesAreLoading      : (state: IncomeState): boolean => state.list.status === StoreStatusEnum.Loading,
        recentIncomes          : (state: IncomeState): IncomeDto[] => state.recentList.data,
        recentIncomesAreLoading: (state: IncomeState): boolean => state.recentList.status === StoreStatusEnum.Loading,
        createIncomeIsLoading  : (state: IncomeState): boolean => state.create.status === StoreStatusEnum.Loading,
        deleteIncomeIsLoading  : (state: IncomeState): boolean => state.delete.status === StoreStatusEnum.Loading,
        updateIncomeIsLoading  : (state: IncomeState): boolean => state.update.status === StoreStatusEnum.Loading
    },
    actions: {
        async loadIncomes(params: IncomeListQueryParamsDto): Promise<void> {
            this.onLoading( 'list', 'create', 'delete', 'update' );
            IncomeListQueryParamsCacheService.save( params );
            const response = await IncomeApiService.getIncomesPaginated( params );
            this.$patch( { list: { data: response.data?.results ?? [], total: response.data?.count ?? 0 } } );
            this.onLoadingFinished( 'list', response.error );
        },
        async loadRecentIncomes(): Promise<void> {
            this.onLoading( 'recentList' );
            const params: IncomeListQueryParamsDto = {
                limit   : 4,
                fromDate: formatDate( moment( new Date() )
                                          .subtract( 7, 'days' )
                                          .toDate() )
            };
            const response = await IncomeApiService.getIncomesPaginated( params );
            this.$patch( { recentList: { data: response.data?.results ?? [] } } );
            this.onLoadingFinished( 'recentList', response.error );
        },
        async createIncome(data: CreateIncomeDto): Promise<void> {
            this.onLoading( 'create' );
            const response = await IncomeApiService.createIncome( data );
            this.onLoadingFinished( 'create', response.error );
        },
        async updateIncome(data: UpdateIncomeDto): Promise<void> {
            this.onLoading( 'update' );
            const response = await IncomeApiService.updateIncome( data );
            this.onLoadingFinished( 'update', response.error );
        },
        async deleteIncome(data: DeleteIncomeDto): Promise<void> {
            this.onLoading( 'delete' );
            const response = await IncomeApiService.deleteIncome( data );
            this.onLoadingFinished( 'delete', response.error );
        },
        onLoading(storeKey: keyof IncomeState, ...storeKeysToClear: (keyof IncomeState)[]) {
            this.$patch(
                {
                    [storeKey]: {
                        status: StoreStatusEnum.Loading
                    },
                    errors    : {
                        [storeKey]: null
                    }
                }
            );
            storeKeysToClear.forEach( storeKey => this.$patch(
                {
                    [storeKey]: {
                        status: StoreStatusEnum.Initial
                    },
                    errors    : {
                        [storeKey]: null
                    }
                }
            ) );
        },
        onLoadingFinished(storeKey: keyof IncomeState, error: ErrorDto | null) {
            this.$patch(
                {
                    [storeKey]: {
                        status: isDefined( error )
                                ? StoreStatusEnum.Failed
                                : StoreStatusEnum.Success
                    },
                    errors    : {
                        [storeKey]: error
                    }
                }
            );
        }
    }
} );
