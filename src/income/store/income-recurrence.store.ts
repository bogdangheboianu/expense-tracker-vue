import { StateErrors, StoreStatusEnum } from '@/config/constants/store.constants';
import { CreateIncomeRecurrenceDto, DeleteIncomeRecurrenceDto, IncomeRecurrenceDto, UpdateIncomeRecurrenceDto, UpdateIncomeRecurrenceStatusDto } from '@/income/dtos/income-recurrence.dtos';
import { IncomeRecurrenceApiService } from '@/income/services/api/income-recurrence-api.service';
import { ErrorDto } from '@/shared/dtos/error.dtos';
import { isDefined } from '@/shared/functions/validation.functions';
import { SetupStepNameEnum } from '@/user/dtos/setup.dtos';
import { useSetupStore } from '@/user/store/setup.store';
import { defineStore } from 'pinia';

export interface IncomeRecurrenceState {
    list: {
        data: IncomeRecurrenceDto[];
        status: StoreStatusEnum
    };
    create: {
        status: StoreStatusEnum
    };
    update: {
        status: StoreStatusEnum
    };
    delete: {
        status: StoreStatusEnum
    };
    updateStatus: {
        status: StoreStatusEnum
    };
    errors: StateErrors<IncomeRecurrenceState>;
}

export const useIncomeRecurrenceStore = defineStore( 'income-recurrence', {
    state  : (): IncomeRecurrenceState => (
        {
            list        : {
                data  : [],
                status: StoreStatusEnum.Initial
            },
            create      : {
                status: StoreStatusEnum.Initial
            },
            update      : {
                status: StoreStatusEnum.Initial
            },
            delete      : {
                status: StoreStatusEnum.Initial
            },
            updateStatus: {
                status: StoreStatusEnum.Initial
            },
            errors      : {
                create      : null,
                list        : null,
                delete      : null,
                update      : null,
                updateStatus: null
            }
        }
    ),
    getters: {
        createIncomeRecurrenceIsLoading      : (state: IncomeRecurrenceState): boolean => state.create.status === StoreStatusEnum.Loading,
        incomeRecurrences                    : (state: IncomeRecurrenceState): IncomeRecurrenceDto[] => state.list.data,
        incomeRecurrencesAreLoading          : (state: IncomeRecurrenceState): boolean => state.list.status === StoreStatusEnum.Loading,
        deleteIncomeRecurrenceIsLoading      : (state: IncomeRecurrenceState): boolean => state.delete.status === StoreStatusEnum.Loading,
        updateIncomeRecurrenceIsLoading      : (state: IncomeRecurrenceState): boolean => state.update.status === StoreStatusEnum.Loading,
        updateIncomeRecurrenceStatusIsLoading: (state: IncomeRecurrenceState): boolean => state.updateStatus.status === StoreStatusEnum.Loading
    },
    actions: {
        async loadAllIncomeRecurrences(): Promise<void> {
            this.onLoading( 'list', 'create', 'delete', 'update', 'updateStatus' );
            const response = await IncomeRecurrenceApiService.getAllIncomeRecurrences();
            this.$patch( { list: { data: response.data ?? [] } } );
            this.onLoadingFinished( 'list', response.error );
        },
        async createIncomeRecurrence(data: CreateIncomeRecurrenceDto, calledFromSetupWizard: boolean = false): Promise<void> {
            this.onLoading( 'create' );
            const response = await IncomeRecurrenceApiService.createIncomeRecurrence( data );
            if( !isDefined( response.error ) ) {
                this.$patch( { list: { data: [ ...this.list.data, response.data! ] } } );
                if( calledFromSetupWizard ) {
                    const setupStore = useSetupStore();
                    setupStore.completeSetupStep( SetupStepNameEnum.IncomeStep );
                }
            }
            this.onLoadingFinished( 'create', response.error );
        },
        async updateIncomeRecurrence(data: UpdateIncomeRecurrenceDto): Promise<void> {
            this.onLoading( 'update' );
            const response = await IncomeRecurrenceApiService.updateIncomeRecurrence( data );
            this.$patch(
                {
                    list: {
                        data: isDefined( response.data )
                              ? this.list.data.map( ir => ir.id !== data.id
                                                          ? ir
                                                          : response.data! )
                              : this.list.data
                    }
                }
            );
            this.onLoadingFinished( 'update', response.error );
        },
        async updateIncomeRecurrenceStatus(data: UpdateIncomeRecurrenceStatusDto): Promise<void> {
            this.onLoading( 'updateStatus' );
            const response = await IncomeRecurrenceApiService.updateIncomeRecurrenceStatus( data );
            this.$patch(
                {
                    list: {
                        data: isDefined( response.data )
                              ? this.list.data.map( ir => ir.id !== data.id
                                                          ? ir
                                                          : response.data! )
                              : this.list.data
                    }
                }
            );
            this.onLoadingFinished( 'updateStatus', response.error );
        },
        async deleteIncomeRecurrence(data: DeleteIncomeRecurrenceDto): Promise<void> {
            this.onLoading( 'delete' );
            const response = await IncomeRecurrenceApiService.deleteIncomeRecurrence( data );
            this.$patch(
                {
                    list: {
                        data: !isDefined( response.error )
                              ? this.list.data.filter( ir => ir.id !== data.id )
                              : this.list.data
                    }
                }
            );
            this.onLoadingFinished( 'delete', response.error );
        },
        onLoading(storeKey: keyof IncomeRecurrenceState, ...storeKeysToClear: (keyof IncomeRecurrenceState)[]) {
            this.$patch(
                {
                    [storeKey]: {
                        status: StoreStatusEnum.Loading
                    },
                    errors    : {
                        [storeKey]: null
                    }
                }
            );
            storeKeysToClear.forEach( storeKey => this.$patch(
                {
                    [storeKey]: {
                        status: StoreStatusEnum.Initial
                    },
                    errors    : {
                        [storeKey]: null
                    }
                }
            ) );
        },
        onLoadingFinished(storeKey: keyof IncomeRecurrenceState, error: ErrorDto | null) {
            this.$patch(
                {
                    [storeKey]: {
                        status: isDefined( error )
                                ? StoreStatusEnum.Failed
                                : StoreStatusEnum.Success
                    },
                    errors    : {
                        [storeKey]: error
                    }
                }
            );
        }
    }
} );
