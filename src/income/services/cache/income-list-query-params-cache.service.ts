import { IncomeListQueryParamsDto } from '@/income/dtos/income.dtos';
import { isDefined } from '@/shared/functions/validation.functions';

export class IncomeListQueryParamsCacheService {

    private static INCOME_LIST_QUERY_PARAMS_STORAGE_KEY = 'income_list_query_params';

    static save(data: IncomeListQueryParamsDto): void {
        this.purge();
        sessionStorage.setItem( this.INCOME_LIST_QUERY_PARAMS_STORAGE_KEY, JSON.stringify( data ) );
    }

    static load(): IncomeListQueryParamsDto | null {
        const flat = sessionStorage.getItem( this.INCOME_LIST_QUERY_PARAMS_STORAGE_KEY );
        return isDefined( flat )
               ? JSON.parse( flat ) as IncomeListQueryParamsDto
               : null;
    }

    static update(data: IncomeListQueryParamsDto): void {
        this.save( data );
    }

    static purge(): void {
        sessionStorage.removeItem( this.INCOME_LIST_QUERY_PARAMS_STORAGE_KEY );
    }
}
