import { apiEndpoints } from '@/core/http/constants/api-endpoints.constants';
import { del, get, post, put } from '@/core/http/functions/http.functions';
import { IApiResponse } from '@/core/http/interfaces/api-response.interface';
import { CreateIncomeDto, DeleteIncomeDto, IncomeDto, IncomeListPaginatedDto, IncomeListQueryParamsDto, UpdateIncomeDto } from '@/income/dtos/income.dtos';

export class IncomeApiService {

    static async getIncomesPaginated(params: IncomeListQueryParamsDto): Promise<IApiResponse<IncomeListPaginatedDto>> {
        return get( apiEndpoints.incomes.list, params );
    }

    static async createIncome(data: CreateIncomeDto): Promise<IApiResponse<IncomeDto>> {
        return post( apiEndpoints.incomes.create, data );
    }

    static async updateIncome(data: UpdateIncomeDto): Promise<IApiResponse<IncomeDto>> {
        return put( apiEndpoints.incomes.update, data );
    }

    static async deleteIncome(data: DeleteIncomeDto): Promise<IApiResponse<void>> {
        return del( apiEndpoints.incomes.delete + `${ data.id }/` );
    }
}
