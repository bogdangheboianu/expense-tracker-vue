import { apiEndpoints } from '@/core/http/constants/api-endpoints.constants';
import { del, get, patch, post, put } from '@/core/http/functions/http.functions';
import { IApiResponse } from '@/core/http/interfaces/api-response.interface';
import { CreateIncomeRecurrenceDto, DeleteIncomeRecurrenceDto, IncomeRecurrenceDto, UpdateIncomeRecurrenceDto, UpdateIncomeRecurrenceStatusDto } from '@/income/dtos/income-recurrence.dtos';

export class IncomeRecurrenceApiService {

    static async createIncomeRecurrence(data: CreateIncomeRecurrenceDto): Promise<IApiResponse<IncomeRecurrenceDto>> {
        return post( apiEndpoints.incomes.createRecurrence, data );
    }

    static async getAllIncomeRecurrences(): Promise<IApiResponse<IncomeRecurrenceDto[]>> {
        return get( apiEndpoints.incomes.recurrences );
    }

    static async deleteIncomeRecurrence(data: DeleteIncomeRecurrenceDto): Promise<IApiResponse<void>> {
        return del( apiEndpoints.incomes.deleteRecurrence + `${ data.id }/` );
    }

    static async updateIncomeRecurrence(data: UpdateIncomeRecurrenceDto): Promise<IApiResponse<IncomeRecurrenceDto>> {
        return put( apiEndpoints.incomes.updateRecurrence, data );
    }

    static async updateIncomeRecurrenceStatus(data: UpdateIncomeRecurrenceStatusDto): Promise<IApiResponse<IncomeRecurrenceDto>> {
        return patch( apiEndpoints.incomes.updateRecurrenceStatus, data );
    }
}
