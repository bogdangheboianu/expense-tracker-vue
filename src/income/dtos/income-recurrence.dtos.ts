import { RecurrenceStatusEnum } from '@/shared/dtos/recurrence.dtos';
import { CurrencyEnum } from '@/shared/enums/currency.enum';
import { WeekDayEnum } from '@/shared/enums/week-day.enum';

export interface IncomeRecurrenceDto {
    id: string;
    name: string;
    source: string | null;
    value: number;
    currency: CurrencyEnum;
    daily: boolean | null;
    weeklyOn: WeekDayEnum | null;
    monthlyOn: number | null;
    status: RecurrenceStatusEnum;
}

export interface CreateIncomeRecurrenceDto {
    name: string;
    source: string | null;
    value: number | null;
    daily: boolean | null;
    weeklyOn: WeekDayEnum | null;
    monthlyOn: number | null;
}

export interface UpdateIncomeRecurrenceDto {
    id: string;
    name: string;
    source: string | null;
    value: number | null;
    daily: boolean | null;
    weeklyOn: WeekDayEnum | null;
    monthlyOn: number | null;
}

export interface DeleteIncomeRecurrenceDto {
    id: string;
}

export interface UpdateIncomeRecurrenceStatusDto {
    id: string;
    status: RecurrenceStatusEnum;
}
