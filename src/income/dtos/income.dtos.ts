import { IncomeRecurrenceDto } from '@/income/dtos/income-recurrence.dtos';
import { CurrencyEnum } from '@/shared/enums/currency.enum';

export interface IncomeDto {
    id: string;
    name: string;
    source: string | null;
    value: number;
    currency: CurrencyEnum;
    date: Date;
    recurrence?: IncomeRecurrenceDto;
}

export interface CreateIncomeDto {
    name: string;
    source: string | null;
    value: number | null;
    date: string | null;
}

export interface UpdateIncomeDto {
    id: string;
    name: string;
    source: string | null;
    value: number | null;
    date: string | null;
}

export interface DeleteIncomeDto {
    id: string;
}

export interface IncomeListPaginatedDto {
    count: number;
    next: string | null;
    prev: string | null;
    results: IncomeDto[];
}

export interface IncomeListQueryParamsDto {
    limit?: number;
    offset?: number;
    fromDate?: string;
    toDate?: string;
    fromValue?: number;
    toValue?: number;
    searchQuery?: string;
}
