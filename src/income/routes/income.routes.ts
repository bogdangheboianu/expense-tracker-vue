import { RouteRecordRaw } from 'vue-router';

export const incomeRoutes: RouteRecordRaw[] = [
    {
        path     : '/incomes',
        name     : 'IncomeList',
        component: () => import('@/income/pages/IncomeListPage.vue'),
        meta     : {
            requiresAuth         : true,
            requiresSetupComplete: true
        }
    }
];
