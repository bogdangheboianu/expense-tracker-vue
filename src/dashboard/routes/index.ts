import { RouteRecordRaw } from 'vue-router';

export const dashboardRoute: RouteRecordRaw = {
    path     : '/dashboard',
    name     : 'Dashboard',
    component: () => import('@/dashboard/pages/DashboardPage.vue'),
    meta     : {
        requiresAuth         : true,
        requiresSetupComplete: true
    }
};
