import { ErrorDto } from '@/shared/dtos/error.dtos';

export enum StoreStatusEnum {
    Initial,
    Loading,
    Success,
    Failed
}

export type StateErrors<S> = Record<Exclude<keyof S, 'errors'>, ErrorDto | null>
