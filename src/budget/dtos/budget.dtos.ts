export interface BudgetDto {
    id: string;
    availableSum: number;
}

export interface CreateBudgetDto {
    availableSum: number | null;
}
