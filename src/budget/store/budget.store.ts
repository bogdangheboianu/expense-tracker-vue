import { CreateBudgetDto } from '@/budget/dtos/budget.dtos';
import { BudgetApiService } from '@/budget/services/api/budget-api.service';
import { StateErrors, StoreStatusEnum } from '@/config/constants/store.constants';
import { ErrorDto } from '@/shared/dtos/error.dtos';
import { isDefined } from '@/shared/functions/validation.functions';
import { SetupStepNameEnum } from '@/user/dtos/setup.dtos';
import { useSetupStore } from '@/user/store/setup.store';
import { useUserStore } from '@/user/store/user.store';
import { defineStore } from 'pinia';

export interface BudgetState {
    create: {
        status: StoreStatusEnum;
    };
    errors: StateErrors<BudgetState>;
}

export const useBudgetStore = defineStore( 'budget', {
    state  : (): BudgetState => (
        {
            create: {
                status: StoreStatusEnum.Initial
            },
            errors: {
                create: null
            }
        }
    ),
    getters: {
        budgetCreateIsLoading: (state: BudgetState): boolean => state.create.status === StoreStatusEnum.Loading
    },
    actions: {
        async createBudget(data: CreateBudgetDto): Promise<void> {
            this.onLoading( 'create' );
            const response = await BudgetApiService.createBudget( data );
            const budget = response.data;
            if( isDefined( budget ) ) {
                const userStore = useUserStore();
                const setupStore = useSetupStore();
                userStore.setUserBudget( budget );
                setupStore.completeSetupStep( SetupStepNameEnum.BudgetStep );
            }
            this.onLoadingFinished( 'create', response.error );
        },
        onLoading(storeKey: keyof BudgetState, ...storeKeysToClear: (keyof BudgetState)[]) {
            this.$patch(
                {
                    [storeKey]: {
                        status: StoreStatusEnum.Loading
                    },
                    errors    : {
                        [storeKey]: null
                    }
                }
            );
            storeKeysToClear.forEach( sk => this.$patch(
                {
                    [sk]  : {
                        status: StoreStatusEnum.Initial
                    },
                    errors: {
                        [sk]: null
                    }
                }
            ) );
        },
        onLoadingFinished(storeKey: keyof BudgetState, error: ErrorDto | null) {
            this.$patch(
                {
                    [storeKey]: {
                        status: isDefined( error )
                                ? StoreStatusEnum.Failed
                                : StoreStatusEnum.Success
                    },
                    errors    : {
                        [storeKey]: error
                    }
                }
            );
        }
    }
} );
