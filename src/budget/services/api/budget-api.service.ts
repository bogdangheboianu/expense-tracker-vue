import { apiEndpoints } from '@/core/http/constants/api-endpoints.constants';
import { post } from '@/core/http/functions/http.functions';
import { IApiResponse } from '@/core/http/interfaces/api-response.interface';
import { BudgetDto, CreateBudgetDto } from '@/budget/dtos/budget.dtos';

export class BudgetApiService {

    static async createBudget(data: CreateBudgetDto): Promise<IApiResponse<BudgetDto>> {
        return post( apiEndpoints.budgets.create, data );
    }
}
