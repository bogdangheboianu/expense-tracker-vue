import { ExpenseListQueryParamsDto } from '@/expense/dtos/expense.dtos';
import { isDefined } from '@/shared/functions/validation.functions';

export class ExpenseListQueryParamsCacheService {

    private static EXPENSE_LIST_QUERY_PARAMS_STORAGE_KEY = 'expense_list_query_params';

    static save(data: ExpenseListQueryParamsDto): void {
        this.purge();
        sessionStorage.setItem( this.EXPENSE_LIST_QUERY_PARAMS_STORAGE_KEY, JSON.stringify( data ) );
    }

    static load(): ExpenseListQueryParamsDto | null {
        const flat = sessionStorage.getItem( this.EXPENSE_LIST_QUERY_PARAMS_STORAGE_KEY );
        return isDefined( flat )
               ? JSON.parse( flat ) as ExpenseListQueryParamsDto
               : null;
    }

    static update(data: ExpenseListQueryParamsDto): void {
        this.save( data );
    }

    static purge(): void {
        sessionStorage.removeItem( this.EXPENSE_LIST_QUERY_PARAMS_STORAGE_KEY );
    }
}
