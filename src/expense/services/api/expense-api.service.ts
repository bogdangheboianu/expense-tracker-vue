import { apiEndpoints } from '@/core/http/constants/api-endpoints.constants';
import { del, get, patch, post, put } from '@/core/http/functions/http.functions';
import { IApiResponse } from '@/core/http/interfaces/api-response.interface';
import { BulkDeleteExpensesDto, BulkExcludeExpensesFromBudgetDto, CreateExpenseDto, DeleteExpenseDto, ExpenseDto, ExpenseListPaginatedDto, ExpenseListQueryParamsDto, UpdateExpenseDto } from '@/expense/dtos/expense.dtos';

export class ExpenseApiService {

    static async getExpensesPaginated(params: ExpenseListQueryParamsDto): Promise<IApiResponse<ExpenseListPaginatedDto>> {
        return get( apiEndpoints.expenses.list, params );
    }

    static async createExpense(data: CreateExpenseDto): Promise<IApiResponse<ExpenseDto>> {
        return post( apiEndpoints.expenses.create, data );
    }

    static async deleteExpense(data: DeleteExpenseDto): Promise<IApiResponse<void>> {
        return del( apiEndpoints.expenses.delete + `${ data.id }/` );
    }

    static async updateExpense(data: UpdateExpenseDto): Promise<IApiResponse<ExpenseDto>> {
        return put( apiEndpoints.expenses.update, data );
    }

    static async bulkDeleteExpenses(data: BulkDeleteExpensesDto): Promise<IApiResponse<void>> {
        return del( apiEndpoints.expenses.bulkDelete + `${ data.ids.join( ',' ) }/` );
    }

    static async bulkExcludeExpensesFromBudget(data: BulkExcludeExpensesFromBudgetDto): Promise<IApiResponse<void>> {
        return patch( apiEndpoints.expenses.bulkExclude, data );
    }
}
