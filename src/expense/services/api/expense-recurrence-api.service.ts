import { apiEndpoints } from '@/core/http/constants/api-endpoints.constants';
import { del, get, patch, post, put } from '@/core/http/functions/http.functions';
import { IApiResponse } from '@/core/http/interfaces/api-response.interface';
import { CreateExpenseRecurrenceDto, DeleteExpenseRecurrenceDto, ExpenseRecurrenceDto, UpdateExpenseRecurrenceDto, UpdateExpenseRecurrenceStatusDto } from '@/expense/dtos/expense-recurrence.dtos';

export class ExpenseRecurrenceApiService {

    static async getAllExpenseRecurrences(): Promise<IApiResponse<ExpenseRecurrenceDto[]>> {
        return get( apiEndpoints.expenses.recurrences );
    }

    static async createExpenseRecurrence(data: CreateExpenseRecurrenceDto): Promise<IApiResponse<ExpenseRecurrenceDto>> {
        return post( apiEndpoints.expenses.createRecurrence, data );
    }

    static async updateExpenseRecurrence(data: UpdateExpenseRecurrenceDto): Promise<IApiResponse<ExpenseRecurrenceDto>> {
        return put( apiEndpoints.expenses.updateRecurrence, data );
    }

    static async deleteExpenseRecurrence(data: DeleteExpenseRecurrenceDto): Promise<IApiResponse<void>> {
        return del( apiEndpoints.expenses.deleteRecurrence + `${ data.id }/` );
    }

    static async updateExpenseRecurrenceStatus(data: UpdateExpenseRecurrenceStatusDto): Promise<IApiResponse<ExpenseRecurrenceDto>> {
        return patch( apiEndpoints.expenses.updateRecurrenceStatus, data );
    }
}
