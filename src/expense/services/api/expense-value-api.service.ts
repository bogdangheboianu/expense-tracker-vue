import { apiEndpoints } from '@/core/http/constants/api-endpoints.constants';
import { del, post, put } from '@/core/http/functions/http.functions';
import { IApiResponse } from '@/core/http/interfaces/api-response.interface';
import { CreateExpenseValueDto, DeleteExpenseValueDto, ExpenseValueDto, UpdateExpenseValueDto } from '@/expense/dtos/expense-value.dtos';

export class ExpenseValueApiService {

    static async createExpenseValue(data: CreateExpenseValueDto): Promise<IApiResponse<ExpenseValueDto>> {
        return post( apiEndpoints.expenses.createValue, data );
    }

    static async updateExpenseValue(data: UpdateExpenseValueDto): Promise<IApiResponse<ExpenseValueDto>> {
        return put( apiEndpoints.expenses.updateValue, data );
    }

    static async deleteExpenseValue(data: DeleteExpenseValueDto): Promise<IApiResponse<void>> {
        return del( apiEndpoints.expenses.deleteValue + `${ data.id }/` );
    }
}
