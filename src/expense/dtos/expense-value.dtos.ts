import { CategoryDto } from '@/category/dtos/category.dtos';

export interface ExpenseValueDto {
    id: string;
    value: number;
    category: CategoryDto;
}

export interface CreateExpenseValueDto {
    expenseId?: string;
    value: number;
    categoryId: string;
    categoryName?: string;
}

export interface UpdateExpenseValueDto {
    id: string;
    value: number | null;
    categoryId: string;
    categoryName?: string
}

export interface DeleteExpenseValueDto {
    id: string;
}
