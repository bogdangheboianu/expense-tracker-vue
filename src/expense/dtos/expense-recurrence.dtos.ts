import { CategoryDto } from '@/category/dtos/category.dtos';
import { SellerDto } from '@/seller/dtos/seller.dtos';
import { RecurrenceStatusEnum } from '@/shared/dtos/recurrence.dtos';
import { CurrencyEnum } from '@/shared/enums/currency.enum';
import { WeekDayEnum } from '@/shared/enums/week-day.enum';

export interface ExpenseRecurrenceDto {
    id: string;
    name: string;
    value: number;
    currency: CurrencyEnum;
    excludeFromBudget: boolean;
    seller: SellerDto | null;
    category: CategoryDto;
    daily: boolean | null;
    weeklyOn: WeekDayEnum | null;
    monthlyOn: number | null;
    status: RecurrenceStatusEnum;
}

export interface CreateExpenseRecurrenceDto {
    name: string;
    value: number | null;
    excludeFromBudget: boolean;
    sellerId?: string | null;
    sellerName?: string | null;
    categoryId: string | null;
    daily: boolean | null;
    weeklyOn: WeekDayEnum | null;
    monthlyOn: number | null;
}

export interface UpdateExpenseRecurrenceDto {
    id: string;
    name: string;
    value: number | null;
    excludeFromBudget: boolean;
    sellerId?: string | null;
    sellerName?: string | null;
    categoryId: string | null;
    daily: boolean | null;
    weeklyOn: WeekDayEnum | null;
    monthlyOn: number | null;
}

export interface DeleteExpenseRecurrenceDto {
    id: string;
}

export interface UpdateExpenseRecurrenceStatusDto {
    id: string;
    status: RecurrenceStatusEnum;
}
