import { ExpenseRecurrenceDto } from '@/expense/dtos/expense-recurrence.dtos';
import { CreateExpenseValueDto, ExpenseValueDto } from '@/expense/dtos/expense-value.dtos';
import { SellerDto } from '@/seller/dtos/seller.dtos';
import { CurrencyEnum } from '@/shared/enums/currency.enum';

export interface ExpenseDto {
    id: string;
    name: string;
    values: ExpenseValueDto[];
    currency: CurrencyEnum;
    date: Date;
    excludeFromBudget: boolean;
    total: number;
    seller: SellerDto | null;
    recurrence: ExpenseRecurrenceDto | null;
}

export interface CreateExpenseDto {
    name: string;
    values: CreateExpenseValueDto[];
    date: string | null;
    excludeFromBudget: boolean;
    sellerId?: string | null;
    sellerName?: string | null;
}

export interface UpdateExpenseDto {
    id: string;
    name: string;
    date: string;
    excludeFromBudget: boolean;
    sellerId?: string | null;
    sellerName?: string | null;
}

export interface DeleteExpenseDto {
    id: string;
}

export interface BulkDeleteExpensesDto {
    ids: string[];
}

export interface BulkExcludeExpensesFromBudgetDto {
    ids: string[];
}

export interface ExpenseListPaginatedDto {
    count: number;
    next: string | null;
    prev: string | null;
    results: ExpenseDto[];
}

export interface ExpenseListQueryParamsDto {
    limit?: number;
    offset?: number;
    fromDate?: string;
    toDate?: string;
    fromValue?: number;
    toValue?: number;
    searchQuery?: string;
}
