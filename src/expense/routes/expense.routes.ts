import { RouteRecordRaw } from 'vue-router';

export const expenseRoutes: RouteRecordRaw[] = [
    {
        path     : '/expenses',
        name     : 'ExpenseList',
        component: () => import('@/expense/pages/ExpenseListPage.vue'),
        meta     : {
            requiresAuth         : true,
            requiresSetupComplete: true
        }
    }
];
