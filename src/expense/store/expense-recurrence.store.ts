import { StateErrors, StoreStatusEnum } from '@/config/constants/store.constants';
import { CreateExpenseRecurrenceDto, DeleteExpenseRecurrenceDto, ExpenseRecurrenceDto, UpdateExpenseRecurrenceDto, UpdateExpenseRecurrenceStatusDto } from '@/expense/dtos/expense-recurrence.dtos';
import { ExpenseRecurrenceApiService } from '@/expense/services/api/expense-recurrence-api.service';
import { ErrorDto } from '@/shared/dtos/error.dtos';
import { isDefined } from '@/shared/functions/validation.functions';
import { defineStore } from 'pinia';

export interface ExpenseRecurrenceState {
    list: {
        data: ExpenseRecurrenceDto[];
        status: StoreStatusEnum;
    };
    create: {
        status: StoreStatusEnum
    };
    update: {
        status: StoreStatusEnum
    };
    updateStatus: {
        status: StoreStatusEnum
    };
    delete: {
        status: StoreStatusEnum
    };
    errors: StateErrors<ExpenseRecurrenceState>;
}

export const useExpenseRecurrenceStore = defineStore( 'expense-recurrence', {
    state  : (): ExpenseRecurrenceState => (
        {
            list        : {
                data  : [],
                status: StoreStatusEnum.Initial
            },
            create      : {
                status: StoreStatusEnum.Initial
            },
            update      : {
                status: StoreStatusEnum.Initial
            },
            updateStatus: {
                status: StoreStatusEnum.Initial
            },
            delete      : {
                status: StoreStatusEnum.Initial
            },
            errors      : {
                list        : null,
                create      : null,
                update      : null,
                updateStatus: null,
                delete      : null
            }
        }
    ),
    getters: {
        expenseRecurrences                    : (state: ExpenseRecurrenceState): ExpenseRecurrenceDto[] => state.list.data,
        expenseRecurrencesAreLoading          : (state: ExpenseRecurrenceState): boolean => state.list.status === StoreStatusEnum.Loading,
        createExpenseRecurrenceIsLoading      : (state: ExpenseRecurrenceState): boolean => state.create.status === StoreStatusEnum.Loading,
        updateExpenseRecurrenceIsLoading      : (state: ExpenseRecurrenceState): boolean => state.update.status === StoreStatusEnum.Loading,
        deleteExpenseRecurrenceIsLoading      : (state: ExpenseRecurrenceState): boolean => state.delete.status === StoreStatusEnum.Loading,
        updateExpenseRecurrenceStatusIsLoading: (state: ExpenseRecurrenceState): boolean => state.updateStatus.status === StoreStatusEnum.Loading
    },
    actions: {
        async loadAllExpenseRecurrences(): Promise<void> {
            this.onLoading( 'list', 'create', 'delete', 'update', 'updateStatus' );
            const response = await ExpenseRecurrenceApiService.getAllExpenseRecurrences();
            this.$patch( { list: { data: response.data ?? [] } } );
            this.onLoadingFinished( 'list', response.error );
        },
        async createExpenseRecurrence(data: CreateExpenseRecurrenceDto): Promise<void> {
            this.onLoading( 'create' );
            const response = await ExpenseRecurrenceApiService.createExpenseRecurrence( data );
            this.$patch( {
                             list: {
                                 data: isDefined( response.data )
                                       ? [ response.data, ...this.list.data ]
                                       : this.list.data
                             }
                         } );
            this.onLoadingFinished( 'create', response.error );
        },
        async updateExpenseRecurrence(data: UpdateExpenseRecurrenceDto): Promise<void> {
            this.onLoading( 'update' );
            const response = await ExpenseRecurrenceApiService.updateExpenseRecurrence( data );
            this.$patch( {
                             list: {
                                 data: isDefined( response.data )
                                       ? this.list.data.map( er => er.id !== data.id
                                                                   ? er
                                                                   : response.data! )
                                       : this.list.data
                             }
                         } );
            this.onLoadingFinished( 'update', response.error );
        },
        async updateExpenseRecurrenceStatus(data: UpdateExpenseRecurrenceStatusDto): Promise<void> {
            this.onLoading( 'updateStatus' );
            const response = await ExpenseRecurrenceApiService.updateExpenseRecurrenceStatus( data );
            this.$patch( {
                             list: {
                                 data: isDefined( response.data )
                                       ? this.list.data.map( er => er.id !== data.id
                                                                   ? er
                                                                   : response.data! )
                                       : this.list.data
                             }
                         } );
            this.onLoadingFinished( 'updateStatus', response.error );
        },
        async deleteExpenseRecurrence(data: DeleteExpenseRecurrenceDto): Promise<void> {
            this.onLoading( 'delete' );
            const response = await ExpenseRecurrenceApiService.deleteExpenseRecurrence( data );
            this.$patch( {
                             list: {
                                 data: !isDefined( response.error )
                                       ? this.list.data.filter( er => er.id !== data.id )
                                       : this.list.data
                             }
                         } );
            this.onLoadingFinished( 'delete', response.error );
        },
        onLoading(storeKey: keyof ExpenseRecurrenceState, ...storeKeysToClear: (keyof ExpenseRecurrenceState)[]) {
            this.$patch(
                {
                    [storeKey]: {
                        status: StoreStatusEnum.Loading
                    },
                    errors    : {
                        [storeKey]: null
                    }
                }
            );
            storeKeysToClear.forEach( storeKey => this.$patch(
                {
                    [storeKey]: {
                        status: StoreStatusEnum.Initial
                    },
                    errors    : {
                        [storeKey]: null
                    }
                }
            ) );
        },
        onLoadingFinished(storeKey: keyof ExpenseRecurrenceState, error: ErrorDto | null) {
            this.$patch(
                {
                    [storeKey]: {
                        status: isDefined( error )
                                ? StoreStatusEnum.Failed
                                : StoreStatusEnum.Success
                    },
                    errors    : {
                        [storeKey]: error
                    }
                }
            );
        }
    }
} );
