import { StateErrors, StoreStatusEnum } from '@/config/constants/store.constants';
import { ExpenseValueDto } from '@/expense/dtos/expense-value.dtos';
import { BulkDeleteExpensesDto, BulkExcludeExpensesFromBudgetDto, CreateExpenseDto, DeleteExpenseDto, ExpenseDto, ExpenseListQueryParamsDto, UpdateExpenseDto } from '@/expense/dtos/expense.dtos';
import { ExpenseApiService } from '@/expense/services/api/expense-api.service';
import { ExpenseListQueryParamsCacheService } from '@/expense/services/cache/expense-list-query-params-cache.service';
import { ErrorDto } from '@/shared/dtos/error.dtos';
import { formatDate } from '@/shared/functions/date.functions';
import { isDefined } from '@/shared/functions/validation.functions';
import moment from 'moment';
import { defineStore } from 'pinia';

export interface ExpenseState {
    list: {
        data: ExpenseDto[];
        total: number;
        status: StoreStatusEnum;
    };
    recentList: {
        data: ExpenseDto[];
        status: StoreStatusEnum;
    };
    create: {
        status: StoreStatusEnum;
    };
    update: {
        status: StoreStatusEnum;
    };
    delete: {
        status: StoreStatusEnum;
    };
    bulkDelete: {
        status: StoreStatusEnum;
    };
    bulkExclude: {
        status: StoreStatusEnum;
    };
    errors: StateErrors<ExpenseState>;
}

export const useExpenseStore = defineStore( 'expense', {
    state  : (): ExpenseState => (
        {
            list       : {
                data  : [],
                total : 0,
                status: StoreStatusEnum.Initial
            },
            recentList : {
                data  : [],
                status: StoreStatusEnum.Initial
            },
            create     : {
                status: StoreStatusEnum.Initial
            },
            update     : {
                status: StoreStatusEnum.Initial
            },
            delete     : {
                status: StoreStatusEnum.Initial
            },
            bulkDelete : {
                status: StoreStatusEnum.Initial
            },
            bulkExclude: {
                status: StoreStatusEnum.Initial
            },
            errors     : {
                list       : null,
                recentList : null,
                create     : null,
                delete     : null,
                update     : null,
                bulkDelete : null,
                bulkExclude: null
            }
        }
    ),
    getters: {
        expenses                    : (state: ExpenseState): ExpenseDto[] => state.list.data,
        totalExpenses               : (state: ExpenseState): number => state.list.total,
        expensesAreLoading          : (state: ExpenseState): boolean => state.list.status === StoreStatusEnum.Loading,
        recentExpenses              : (state: ExpenseState): ExpenseDto[] => state.recentList.data,
        recentExpensesAreLoading    : (state: ExpenseState): boolean => state.recentList.status === StoreStatusEnum.Loading,
        createExpenseIsLoading      : (state: ExpenseState): boolean => state.create.status === StoreStatusEnum.Loading,
        createExpenseIsSuccess      : (state: ExpenseState): boolean => state.create.status === StoreStatusEnum.Success,
        deleteExpenseIsLoading      : (state: ExpenseState): boolean => state.delete.status === StoreStatusEnum.Loading,
        updateExpenseIsLoading      : (state: ExpenseState): boolean => state.update.status === StoreStatusEnum.Loading,
        expenseById                 : (state: ExpenseState) => (expenseId: string): ExpenseDto => state.list.data.find( e => e.id === expenseId )!,
        bulkDeleteExpensesIsLoading : (state: ExpenseState): boolean => state.bulkDelete.status === StoreStatusEnum.Loading,
        bulkExcludeExpensesIsLoading: (state: ExpenseState): boolean => state.bulkExclude.status === StoreStatusEnum.Loading
    },
    actions: {
        async loadExpenses(params: ExpenseListQueryParamsDto): Promise<void> {
            this.onLoading( 'list', 'create', 'delete', 'update' );
            ExpenseListQueryParamsCacheService.save( params );
            const response = await ExpenseApiService.getExpensesPaginated( params );
            this.$patch( { list: { data: response.data?.results ?? [], total: response.data?.count ?? 0 } } );
            this.onLoadingFinished( 'list', response.error );
        },
        async loadRecentExpenses(): Promise<void> {
            this.onLoading( 'recentList' );
            const params: ExpenseListQueryParamsDto = {
                limit   : 4,
                fromDate: formatDate( moment( new Date() )
                                          .subtract( 7, 'days' )
                                          .toDate() )
            };
            const response = await ExpenseApiService.getExpensesPaginated( params );
            this.$patch( { recentList: { data: response.data?.results ?? [] } } );
            this.onLoadingFinished( 'recentList', response.error );
        },
        async createExpense(data: CreateExpenseDto): Promise<void> {
            this.onLoading( 'create' );
            const response = await ExpenseApiService.createExpense( data );
            this.onLoadingFinished( 'create', response.error );
        },
        async updateExpense(data: UpdateExpenseDto): Promise<void> {
            this.onLoading( 'update' );
            const response = await ExpenseApiService.updateExpense( data );
            this.onLoadingFinished( 'update', response.error );
        },
        async deleteExpense(data: DeleteExpenseDto): Promise<void> {
            this.onLoading( 'delete' );
            const response = await ExpenseApiService.deleteExpense( data );
            if( !isDefined( response.error ) ) {
                this.$patch( { list: { data: this.list.data.filter( e => e.id !== data.id ) } } );
            }
            this.onLoadingFinished( 'delete', response.error );
        },
        async bulkDeleteExpenses(data: BulkDeleteExpensesDto): Promise<void> {
            this.onLoading( 'bulkDelete' );
            const response = await ExpenseApiService.bulkDeleteExpenses( data );
            if( !isDefined( response.error ) ) {
                this.$patch(
                    {
                        list: {
                            data: this.list.data.filter( expense => !data.ids.some( id => id === expense.id ) )
                        }
                    }
                );
            }
            this.onLoadingFinished( 'bulkDelete', response.error );
        },
        async bulkExcludeExpensesFromBudget(data: BulkExcludeExpensesFromBudgetDto): Promise<void> {
            this.onLoading( 'bulkExclude' );
            const response = await ExpenseApiService.bulkExcludeExpensesFromBudget( data );
            if( isDefined( response.data ) ) {
                this.$patch(
                    {
                        list: {
                            data: this.list.data.map( expense => !data.ids.includes( expense.id )
                                                                 ? expense
                                                                 : { ...expense, excludeFromBudget: true } )
                        }
                    }
                );
            }
            this.onLoadingFinished( 'bulkExclude', response.error );
        },
        addExpenseValueToExpense(newExpenseValue: ExpenseValueDto, expenseId: string): void {
            this.$patch(
                {
                    list: {
                        data: this.list.data.map( expense => {
                            return expense.id !== expenseId
                                   ? expense
                                   : { ...expense, values: [ ...expense.values, newExpenseValue ] };
                        } )
                    }
                }
            );
        },
        updateExpenseValueOfExpense(updatedExpenseValue: ExpenseValueDto, expenseId: string): void {
            this.$patch(
                {
                    list: {
                        data: this.list.data.map( expense => {
                            return expense.id !== expenseId
                                   ? expense
                                   : {
                                    ...expense, values: expense.values.map( ev => ev.id !== updatedExpenseValue.id
                                                                                  ? ev
                                                                                  : updatedExpenseValue )
                                };
                        } )
                    }
                }
            );
        },
        removeExpenseValueFromExpense(expenseValueIdToRemove: string, expenseId: string): void {
            this.$patch(
                {
                    list: {
                        data: this.list.data.map( expense => {
                            return expense.id !== expenseId
                                   ? expense
                                   : {
                                    ...expense, values: expense.values.filter( ev => ev.id !== expenseValueIdToRemove )
                                };
                        } )
                    }
                }
            );
        },
        onLoading(storeKey: keyof ExpenseState, ...storeKeysToClear: (keyof ExpenseState)[]) {
            this.$patch(
                {
                    [storeKey]: {
                        status: StoreStatusEnum.Loading
                    },
                    errors    : {
                        [storeKey]: null
                    }
                }
            );
            storeKeysToClear.forEach( sk => this.$patch(
                {
                    [sk]  : {
                        status: StoreStatusEnum.Initial
                    },
                    errors: {
                        [sk]: null
                    }
                }
            ) );
        },
        onLoadingFinished(storeKey: keyof ExpenseState, error: ErrorDto | null) {
            this.$patch(
                {
                    [storeKey]: {
                        status: isDefined( error )
                                ? StoreStatusEnum.Failed
                                : StoreStatusEnum.Success
                    },
                    errors    : {
                        [storeKey]: error
                    }
                }
            );
        }
    }
} );
