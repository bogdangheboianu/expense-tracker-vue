import { StateErrors, StoreStatusEnum } from '@/config/constants/store.constants';
import { CreateExpenseValueDto, DeleteExpenseValueDto, UpdateExpenseValueDto } from '@/expense/dtos/expense-value.dtos';
import { ExpenseValueApiService } from '@/expense/services/api/expense-value-api.service';
import { useExpenseStore } from '@/expense/store/expense.store';
import { ErrorDto } from '@/shared/dtos/error.dtos';
import { isDefined } from '@/shared/functions/validation.functions';
import { defineStore } from 'pinia';

export interface ExpenseValueState {
    create: {
        status: StoreStatusEnum;
    };
    update: {
        status: StoreStatusEnum;
    };
    delete: {
        status: StoreStatusEnum;
    };
    errors: StateErrors<ExpenseValueState>;
}

export const useExpenseValueStore = defineStore( 'expense-value', {
    state  : (): ExpenseValueState => (
        {
            create: {
                status: StoreStatusEnum.Initial
            },
            update: {
                status: StoreStatusEnum.Initial
            },
            delete: {
                status: StoreStatusEnum.Initial
            },
            errors: {
                create: null,
                update: null,
                delete: null
            }
        }
    ),
    getters: {
        createExpenseValueIsLoading: (state: ExpenseValueState): boolean => state.create.status === StoreStatusEnum.Loading,
        updateExpenseValueIsLoading: (state: ExpenseValueState): boolean => state.update.status === StoreStatusEnum.Loading,
        deleteExpenseValueIsLoading: (state: ExpenseValueState): boolean => state.delete.status === StoreStatusEnum.Loading
    },
    actions: {
        async createExpenseValue(data: CreateExpenseValueDto): Promise<void> {
            this.onLoading( 'create', 'update', 'delete' );
            const response = await ExpenseValueApiService.createExpenseValue( data );
            if( isDefined( response.data ) ) {
                const expenseStore = useExpenseStore();
                expenseStore.addExpenseValueToExpense( response.data!, data.expenseId! );
            }
            this.onLoadingFinished( 'create', response.error );
        },
        async updateExpenseValue(data: UpdateExpenseValueDto, expenseId: string): Promise<void> {
            this.onLoading( 'update', 'create', 'delete' );
            const response = await ExpenseValueApiService.updateExpenseValue( data );
            if( isDefined( response.data ) ) {
                const expenseStore = useExpenseStore();
                expenseStore.updateExpenseValueOfExpense( response.data!, expenseId );
            }
            this.onLoadingFinished( 'update', response.error );
        },
        async deleteExpenseValue(data: DeleteExpenseValueDto, expenseId: string): Promise<void> {
            this.onLoading( 'delete', 'create', 'update' );
            const response = await ExpenseValueApiService.deleteExpenseValue( data );
            if( !isDefined( response.error ) ) {
                const expenseStore = useExpenseStore();
                expenseStore.removeExpenseValueFromExpense( data.id, expenseId );
            }
            this.onLoadingFinished( 'delete', response.error );
        },
        onLoading(storeKey: keyof ExpenseValueState, ...storeKeysToClear: (keyof ExpenseValueState)[]) {
            this.$patch(
                {
                    [storeKey]: {
                        status: StoreStatusEnum.Loading
                    },
                    errors    : {
                        [storeKey]: null
                    }
                }
            );
            storeKeysToClear.forEach( sk => this.$patch(
                {
                    [sk]  : {
                        status: StoreStatusEnum.Initial
                    },
                    errors: {
                        [sk]: null
                    }
                }
            ) );
        },
        onLoadingFinished(storeKey: keyof ExpenseValueState, error: ErrorDto | null) {
            this.$patch(
                {
                    [storeKey]: {
                        status: isDefined( error )
                                ? StoreStatusEnum.Failed
                                : StoreStatusEnum.Success
                    },
                    errors    : {
                        [storeKey]: error
                    }
                }
            );
        }
    }
} );
