import { router } from '@/core/router';
import 'element-plus/dist/index.css';
import { createPinia } from 'pinia';
import { createApp } from 'vue';
import '../styles/basic-styles.css';
import '../styles/custom-styles.css';
import '../styles/element-plus-override.css';
import App from './App.vue';

const app = createApp( App );

app.use( createPinia() );
app.use( router );

app.mount( '#app' );
