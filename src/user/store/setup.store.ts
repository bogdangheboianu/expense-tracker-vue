import { StateErrors, StoreStatusEnum } from '@/config/constants/store.constants';
import { ErrorDto } from '@/shared/dtos/error.dtos';
import { isDefined } from '@/shared/functions/validation.functions';
import { SetupDto, SetupStepDto, SetupStepNameEnum } from '@/user/dtos/setup.dtos';
import { SetupApiService } from '@/user/services/api/setup-api.service';
import { UserCacheService } from '@/user/services/cache/user-cache.service';
import { useUserStore } from '@/user/store/user.store';
import { defineStore } from 'pinia';

export interface SetupState {
    setup: SetupDto | null;
    setupSteps: {
        list: SetupStepDto[];
        status: StoreStatusEnum;
    };
    skip: {
        status: StoreStatusEnum;
    };
    errors: StateErrors<SetupState>;
}

export const useSetupStore = defineStore( 'setup', {
    state  : (): SetupState => (
        {
            setup     : UserCacheService.load()?.setup ?? null,
            setupSteps: {
                list  : [],
                status: StoreStatusEnum.Initial
            },
            skip      : {
                status: StoreStatusEnum.Initial
            },
            errors    : {
                setup     : null,
                setupSteps: null,
                skip      : null
            }
        }
    ),
    getters: {
        setupStepsToBeCompleted: (state: SetupState) => state.setupSteps.list.filter( step => !step.isComplete && !step.isSkipped ),
        firstStepToBeCompleted(): SetupStepDto | undefined {
            return this.setupStepsToBeCompleted[0];
        },
        setupIsComplete       : (state: SetupState) => state.setup?.isComplete ?? false,
        setupStepByName       : (state: SetupState) => (name: SetupStepNameEnum): SetupStepDto | undefined => state.setupSteps.list.find( step => step.name === name ),
        skipSetupStepIsLoading: (state: SetupState): boolean => state.skip.status === StoreStatusEnum.Loading,
        setupStepList         : (state: SetupState): SetupStepDto[] => state.setupSteps.list,
        setupStepListIsLoading: (state: SetupState): boolean => state.setupSteps.status === StoreStatusEnum.Loading
    },
    actions: {
        async loadSetupSteps(): Promise<void> {
            const userStore = useUserStore();
            const setup = userStore.loggedUser.data?.setup;
            if( isDefined( setup ) ) {
                this.onLoading( 'setupSteps', 'skip' );
                const response = await SetupApiService.getSetupStepsBySetupId( setup.id );
                this.$patch( { setupSteps: { list: response.data ?? [] } } );
                this.onLoadingFinished( 'setupSteps', response.error );
            }
        },
        async skipSetupStep(setupStepId: string): Promise<void> {
            this.onLoading( 'skip', 'setupSteps' );
            const response = await SetupApiService.skipSetupStep( setupStepId );
            if( !isDefined( response.error ) ) {
                this.$patch(
                    {
                        setupSteps: {
                            ...this.setupSteps,
                            list: this.setupSteps.list.map( step => step.id === setupStepId
                                                                    ? { ...step, isSkipped: true }
                                                                    : step )
                        }
                    } );
                if( this.setupStepsToBeCompleted.length === 0 ) {
                    this.completeSetup();
                }
            }
            this.onLoadingFinished( 'skip', response.error );
        },
        completeSetupStep(setupStepName: SetupStepNameEnum): void {
            this.$patch(
                {
                    setupSteps: {
                        ...this.setupSteps,
                        list: this.setupSteps.list.map( step => step.name === setupStepName
                                                                ? { ...step, isComplete: true }
                                                                : step )
                    }
                } );
            if( this.setupStepsToBeCompleted.length === 0 ) {
                this.completeSetup();
            }
        },
        completeSetup(): void {
            this.$patch( { setup: { ...this.setup, isComplete: true } } );
            useUserStore()
                .completeUserSetup();
        },
        onLoading(storeKey: keyof SetupState, ...storeKeysToClear: (keyof SetupState)[]) {
            this.$patch(
                {
                    [storeKey]: {
                        status: StoreStatusEnum.Loading
                    },
                    errors    : {
                        [storeKey]: null
                    }
                }
            );
            storeKeysToClear.forEach( sk => this.$patch(
                {
                    [sk]  : {
                        status: StoreStatusEnum.Initial
                    },
                    errors: {
                        [sk]: null
                    }
                }
            ) );
        },
        onLoadingFinished(storeKey: keyof SetupState, error: ErrorDto | null) {
            this.$patch(
                {
                    [storeKey]: {
                        status: isDefined( error )
                                ? StoreStatusEnum.Failed
                                : StoreStatusEnum.Success
                    },
                    errors    : {
                        [storeKey]: error
                    }
                }
            );
        }
    }
} );
