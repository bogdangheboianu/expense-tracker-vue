import { BudgetDto } from '@/budget/dtos/budget.dtos';
import { StateErrors, StoreStatusEnum } from '@/config/constants/store.constants';
import { ErrorDto } from '@/shared/dtos/error.dtos';
import { isDefined } from '@/shared/functions/validation.functions';
import { UserSettingsDto } from '@/user/dtos/settings.dtos';
import { RegisterUserDto, UserDto } from '@/user/dtos/user.dtos';
import { UserApiService } from '@/user/services/api/user-api.service';
import { UserCacheService } from '@/user/services/cache/user-cache.service';
import { useSetupStore } from '@/user/store/setup.store';
import { defineStore } from 'pinia';

export interface UserState {
    register: {
        status: StoreStatusEnum;
    };
    loggedUser: {
        data: UserDto | null;
        status: StoreStatusEnum
    };
    errors: StateErrors<UserState>;
}

export const useUserStore = defineStore( 'user', {
    state  : (): UserState => (
        {
            register  : {
                status: StoreStatusEnum.Initial
            },
            loggedUser: {
                data  : UserCacheService.load(),
                status: StoreStatusEnum.Initial
            },
            errors    : {
                register  : null,
                loggedUser: null
            }
        }
    ),
    getters: {
        loggedUserIsDefined  : (state: UserState): boolean => isDefined( state.loggedUser.data ),
        loggedUserIsLoading  : (state: UserState): boolean => state.loggedUser.status === StoreStatusEnum.Loading,
        loggedUsername       : (state: UserState): string | undefined => state.loggedUser.data?.username,
        budget               : (state: UserState): BudgetDto | undefined | null => state.loggedUser.data?.budget,
        userSettings         : (state: UserState): UserSettingsDto | undefined => state.loggedUser.data?.settings,
        registerUserIsLoading: (state: UserState): boolean => state.register.status === StoreStatusEnum.Loading,
        registerUserError    : (state: UserState): ErrorDto | null => state.errors.register
    },
    actions: {
        async registerUser(data: RegisterUserDto): Promise<void> {
            this.onLoading( 'register', 'loggedUser' );
            const response = await UserApiService.registerUser( data );
            this.onLoadingFinished( 'register', response.error );
        },
        async loadCurrentUser() {
            this.onLoading( 'loggedUser' );
            const response = await UserApiService.getCurrentUser();
            const user = response.data;
            this.$patch( { loggedUser: { data: user } } );
            if( isDefined( user ) ) {
                UserCacheService.save( user, true );
                const setupStore = useSetupStore();
                setupStore.setup = user.setup;
            }
            this.onLoadingFinished( 'loggedUser', response.error );
        },
        setUserBudget(budget: BudgetDto): void {
            if( !this.loggedUserIsDefined ) {
                return;
            }
            this.$patch(
                {
                    loggedUser: {
                        data: {
                            ...this.loggedUser.data,
                            budget
                        }
                    }
                }
            );
            UserCacheService.update( this.loggedUser.data! );
        },
        completeUserSetup(): void {
            if( !this.loggedUserIsDefined ) {
                return;
            }
            this.$patch(
                {
                    loggedUser: {
                        data: {
                            ...this.loggedUser.data,
                            setup: {
                                ...this.loggedUser.data!.setup,
                                isComplete: true
                            }
                        }
                    }
                }
            );
            UserCacheService.update( this.loggedUser.data! );
        },
        onLogout() {
            this.$reset();
            UserCacheService.purge();
        },
        onLoading(storeKey: keyof UserState, ...storeKeysToClear: (keyof UserState)[]) {
            this.$patch(
                {
                    [storeKey]: {
                        status: StoreStatusEnum.Loading
                    },
                    errors    : {
                        [storeKey]: null
                    }
                }
            );
            storeKeysToClear.forEach( sk => this.$patch(
                {
                    [sk]  : {
                        status: StoreStatusEnum.Initial
                    },
                    errors: {
                        [sk]: null
                    }
                }
            ) );
        },
        onLoadingFinished(storeKey: keyof UserState, error: ErrorDto | null) {
            this.$patch(
                {
                    [storeKey]: {
                        status: isDefined( error )
                                ? StoreStatusEnum.Failed
                                : StoreStatusEnum.Success
                    },
                    errors    : {
                        [storeKey]: error
                    }
                }
            );
        }
    }
} );
