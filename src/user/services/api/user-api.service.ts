import { apiEndpoints } from '@/core/http/constants/api-endpoints.constants';
import { get, post } from '@/core/http/functions/http.functions';
import { IApiResponse } from '@/core/http/interfaces/api-response.interface';
import { RegisterUserDto, UserDto } from '@/user/dtos/user.dtos';

export class UserApiService {

    static async registerUser(data: RegisterUserDto): Promise<IApiResponse<UserDto>> {
        return post( apiEndpoints.users.create, data, false );
    }

    static async getCurrentUser(): Promise<IApiResponse<UserDto>> {
        return get( apiEndpoints.users.me );
    }
}
