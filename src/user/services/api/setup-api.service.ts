import { apiEndpoints } from '@/core/http/constants/api-endpoints.constants';
import { get, patch } from '@/core/http/functions/http.functions';
import { IApiResponse } from '@/core/http/interfaces/api-response.interface';
import { SetupStepDto } from '@/user/dtos/setup.dtos';

export class SetupApiService {

    static async getSetupStepsBySetupId(setupId: string): Promise<IApiResponse<SetupStepDto[]>> {
        return get( `${ apiEndpoints.setup.steps }?setup_id=${ setupId }` );
    }

    static async skipSetupStep(setupStepId: string): Promise<IApiResponse<void>> {
        return patch( `${ apiEndpoints.setup.steps }${ setupStepId }/skip/` );
    }
}
