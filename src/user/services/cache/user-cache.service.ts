import { UserDto } from '@/user/dtos/user.dtos';
import { isDefined } from '@/shared/functions/validation.functions';

export class UserCacheService {

    private static USER_STORAGE_KEY = 'user';

    static save(data: UserDto, persist: boolean): void {
        this.purge();
        const storage = persist
                        ? localStorage
                        : sessionStorage;
        storage.setItem( this.USER_STORAGE_KEY, JSON.stringify( data ) );
    }

    static load(): UserDto | null {
        const flat = localStorage.getItem( this.USER_STORAGE_KEY ) ?? sessionStorage.getItem( this.USER_STORAGE_KEY );
        return isDefined( flat )
               ? JSON.parse( flat ) as UserDto
               : null;
    }

    static update(user: UserDto): void {
        this.save( user, true );
    }

    static purge(): void {
        localStorage.removeItem( this.USER_STORAGE_KEY );
        sessionStorage.removeItem( this.USER_STORAGE_KEY );
    }
}
