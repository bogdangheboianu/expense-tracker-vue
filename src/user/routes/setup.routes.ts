import { RouteRecordRaw } from 'vue-router';

export const setupRoutes: RouteRecordRaw[] = [
    {
        path     : '/setup',
        name     : 'Setup',
        component: () => import('@/user/pages/account-setup-pages/SetupPage.vue'),
        children : [
            {
                path     : 'budget',
                name     : 'SetupBudget',
                component: () => import('@/user/pages/account-setup-pages/steps/BalanceSetupStepTab.vue')
            },
            {
                path     : 'income',
                name     : 'SetupIncome',
                component: () => import('@/user/pages/account-setup-pages/steps/SalarySetupStepTab.vue')
            }
        ],
        meta     : {
            requiresAuth: true
        }
    }
];
