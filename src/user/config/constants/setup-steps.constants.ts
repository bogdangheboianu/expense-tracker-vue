import { SetupStepNameEnum } from '@/user/dtos/setup.dtos';

export const SetupStepNameToTabComponent: { [K in SetupStepNameEnum]: string } = {
    [SetupStepNameEnum.BudgetStep]: 'SetupBudget',
    [SetupStepNameEnum.IncomeStep]: 'SetupIncome'
};
