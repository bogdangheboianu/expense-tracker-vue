import { BudgetDto } from '@/budget/dtos/budget.dtos';
import { UserSettingsDto } from '@/user/dtos/settings.dtos';
import { SetupDto } from '@/user/dtos/setup.dtos';

export interface UserDto {
    id: string;
    username: string;
    email: string | null;
    firstName: string | null;
    lastName: string | null;
    budget: BudgetDto | null;
    setup: SetupDto;
    settings: UserSettingsDto;
}

export interface RegisterUserDto {
    username: string;
    password: string;
}
