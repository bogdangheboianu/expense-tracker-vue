export enum SetupStepNameEnum {
    BudgetStep = 'budget_step',
    IncomeStep = 'income_step'
}

export interface SetupStepDto {
    id: string;
    name: SetupStepNameEnum;
    isComplete: boolean;
    isOptional: boolean;
    isSkipped: boolean;
}

export interface SetupDto {
    id: string;
    isComplete: boolean;
}
