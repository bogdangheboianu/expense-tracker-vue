export interface UserSettingsDto {
    id: string;
    monthStartsOn: number;
}
