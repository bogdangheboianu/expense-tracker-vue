import { LoginFormData } from '@/auth/components/LoginForm.vue';
import { JwtTokensDto, LoginResponseDto } from '@/auth/dtos/auth.dtos';
import { AuthApiService } from '@/auth/services/api/auth-api.service';
import { JwtCacheService } from '@/auth/services/cache/jwt-cache.service';
import { StateErrors, StoreStatusEnum } from '@/config/constants/store.constants';
import { ErrorDto } from '@/shared/dtos/error.dtos';
import { isDefined } from '@/shared/functions/validation.functions';
import { useUserStore } from '@/user/store/user.store';
import { defineStore } from 'pinia';

export interface AuthState {
    tokens: JwtTokensDto | null;
    login: {
        status: StoreStatusEnum;
    };
    errors: StateErrors<AuthState>;
}

export const useAuthStore = defineStore( 'auth', {
    state  : (): AuthState => (
        {
            tokens: JwtCacheService.load(),
            login : {
                status: StoreStatusEnum.Initial
            },
            errors: {
                login : null,
                tokens: null
            }
        }
    ),
    getters: {
        accessToken: (state: AuthState): string | undefined => state.tokens?.access,
        isAuthenticated(): boolean {
            return isDefined( this.accessToken ) && JwtCacheService.isAccessTokenValid();
        },
        loginIsLoading: (state: AuthState): boolean => state.login.status === StoreStatusEnum.Loading,
        loginError    : (state: AuthState): ErrorDto | null => state.errors.login
    },
    actions: {
        async login(data: LoginFormData): Promise<LoginResponseDto | null> {
            this.onLoading( 'login' );
            const response = await AuthApiService.login( data );
            if( isDefined( response.data ) ) {
                this.$patch( { tokens: { ...response.data } } );
                JwtCacheService.save( response.data!, data.keepLoggedIn );
                const userStore = useUserStore();
                await userStore.loadCurrentUser();
            }
            this.onLoadingFinished( 'login', response.error );
            return response.data;
        },
        logout() {
            this.$reset();
            useUserStore()
                .onLogout();
            JwtCacheService.purge();
            window.location.href = '/';
        },
        onLoading(storeKey: keyof AuthState, ...storeKeysToClear: (keyof AuthState)[]) {
            this.$patch(
                {
                    [storeKey]: {
                        status: StoreStatusEnum.Loading
                    },
                    errors    : {
                        [storeKey]: null
                    }
                }
            );
            storeKeysToClear.forEach( sk => this.$patch(
                {
                    [sk]  : {
                        status: StoreStatusEnum.Initial
                    },
                    errors: {
                        [sk]: null
                    }
                }
            ) );
        },
        onLoadingFinished(storeKey: keyof AuthState, error: ErrorDto | null) {
            this.$patch(
                {
                    [storeKey]: {
                        status: isDefined( error )
                                ? StoreStatusEnum.Failed
                                : StoreStatusEnum.Success
                    },
                    errors    : {
                        [storeKey]: error
                    }
                }
            );
        }
    }
} );
