import { apiEndpoints } from '@/core/http/constants/api-endpoints.constants';
import { post } from '@/core/http/functions/http.functions';
import { IApiResponse } from '@/core/http/interfaces/api-response.interface';
import { LoginRequestDto, LoginResponseDto } from '@/auth/dtos/auth.dtos';

export class AuthApiService {

    static async login(data: LoginRequestDto): Promise<IApiResponse<LoginResponseDto>> {
        return post( apiEndpoints.auth.login, data, false );
    }
}
