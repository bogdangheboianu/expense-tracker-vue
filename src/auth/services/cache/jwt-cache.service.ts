import { JwtTokensDto } from '@/auth/dtos/auth.dtos';
import { isDefined } from '@/shared/functions/validation.functions';

export class JwtCacheService {

    private static JWTS_STORAGE_KEY = 'jwts';

    static save(data: JwtTokensDto, persist: boolean): void {
        this.purge();
        const storage = persist
                        ? localStorage
                        : sessionStorage;
        storage.setItem( this.JWTS_STORAGE_KEY, JSON.stringify( data ) );
    }

    static load(): JwtTokensDto | null {
        const flat = localStorage.getItem( this.JWTS_STORAGE_KEY ) ?? sessionStorage.getItem( this.JWTS_STORAGE_KEY );
        return isDefined( flat )
               ? JSON.parse( flat ) as JwtTokensDto
               : null;
    }

    static getAccessToken(): string | null {
        return this.load()?.access ?? null;
    }

    static isAccessTokenValid(): boolean {
        return isDefined( this.getAccessToken() );
    }

    static updateAccessToken(newAccessToken: string): void {
        const tokens = this.load();
        if( isDefined( tokens ) ) {
            tokens.access = newAccessToken;
            //TODO
            this.save( tokens, true );
        }
    }

    static purge(): void {
        localStorage.removeItem( this.JWTS_STORAGE_KEY );
        sessionStorage.removeItem( this.JWTS_STORAGE_KEY );
    }
}
