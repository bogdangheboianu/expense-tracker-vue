import { RouteRecordRaw } from 'vue-router';

export const authRoutes: RouteRecordRaw[] = [
    {
        path     : '/auth',
        name     : 'Auth',
        component: () => import('@/auth/pages/AuthPage.vue')
    }
];
