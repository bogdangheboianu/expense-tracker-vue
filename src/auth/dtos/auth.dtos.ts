export interface LoginRequestDto {
    username: string;
    password: string;
}

export interface LoginResponseDto {
    access: string;
    refresh: string;
}

export interface JwtTokensDto {
    access: string;
    refresh: string;
}

const ceva = {
    name: 'ceva',
    altNameAlt: 'altceva'
}
