import { CategoryDto } from '@/category/dtos/category.dtos';
import { CategoryInputProps } from '@/category/interfaces/category-input-props.interface';
import { FormFieldTypeEnum } from '@/shared/enums/form-field-type.enum';
import { buildInputProps } from '@/shared/functions/input-props.functions';

export const categoryInputProps = <F>(label: string, prop: keyof F, value: CategoryDto | null = null, required: boolean = true): CategoryInputProps<F> => (
    {
        ...buildInputProps( FormFieldTypeEnum.Seller, label, prop, value?.id, required ),
        category: value ?? undefined
    }
);
