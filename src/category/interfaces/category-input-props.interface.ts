import { CategoryDto } from '@/category/dtos/category.dtos';
import { InputProps } from '@/shared/interfaces/input-props/input-props.interface';

export interface CategoryInputProps<F> extends InputProps<string, F> {
    category?: CategoryDto;
}
