import { CategoryApiService } from '@/category/services/api/category-api.service';
import { CategoryDto, CategoryTypeEnum } from '@/category/dtos/category.dtos';
import { ErrorDto } from '@/shared/dtos/error.dtos';
import { isDefined } from '@/shared/functions/validation.functions';
import { defineStore } from 'pinia';

export interface CategoryState {
    list: CategoryDto[];
    loading: boolean;
    success: boolean;
    error: ErrorDto | null;
}

export const useCategoryStore = defineStore( 'category', {
    state  : (): CategoryState => (
        {
            list   : [],
            loading: false,
            success: false,
            error  : null
        }
    ),
    actions: {
        async loadAllExpenseCategories(): Promise<CategoryDto[]> {
            this.onLoading();
            const response = await CategoryApiService.getAllCategories( { type: CategoryTypeEnum.ExpenseCategory } );
            const categories = response.data ?? [];
            this.$patch( { list: categories } );
            this.onLoadingFinished( response.error );
            return categories;
        },
        onLoading(): void {
            this.$patch( { loading: true, success: false, error: null } );
        },
        onLoadingFinished(error: ErrorDto | null): void {
            this.$patch( { loading: false, success: !isDefined( error ), error } );
        }
    }
} );
