export enum CategoryTypeEnum {
    ExpenseCategory = 'expense_category',
    IncomeCategory  = 'income_category'
}

export interface CategoryDto {
    id: string;
    name: string;
    slug: string;
    type: CategoryTypeEnum;
}

export interface CategoryListQueryParamsDto {
    type?: CategoryTypeEnum;
}
