import { apiEndpoints } from '@/core/http/constants/api-endpoints.constants';
import { get } from '@/core/http/functions/http.functions';
import { IApiResponse } from '@/core/http/interfaces/api-response.interface';
import { CategoryDto, CategoryListQueryParamsDto } from '@/category/dtos/category.dtos';

export class CategoryApiService {

    static async getAllCategories(params: CategoryListQueryParamsDto): Promise<IApiResponse<CategoryDto[]>> {
        return get( apiEndpoints.categories.list, params );
    }
}
